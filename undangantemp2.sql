-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2019 at 10:38 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `undangantemp2`
--

-- --------------------------------------------------------

--
-- Table structure for table `galleri`
--

CREATE TABLE `galleri` (
  `id` int(11) NOT NULL,
  `image` varchar(225) NOT NULL,
  `title` varchar(225) NOT NULL,
  `subtitle` varchar(335) NOT NULL,
  `upload_date` datetime NOT NULL,
  `kode_internal` varchar(9) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galleri`
--

INSERT INTO `galleri` (`id`, `image`, `title`, `subtitle`, `upload_date`, `kode_internal`, `status`) VALUES
(4, 'photo_DI7vpdYfgMXG_1552979010.jpg', '1', '1', '2019-03-19 14:03:43', '', 0),
(6, 'photo_cu7vFjpJVMCs_1552980156.jpg', '12', '23', '2019-03-19 14:22:45', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `image_temp`
--

CREATE TABLE `image_temp` (
  `id` int(11) NOT NULL,
  `photo` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image_temp`
--

INSERT INTO `image_temp` (`id`, `photo`, `created_at`) VALUES
(4, 'photo_Yh9XvWU0mOlQ_1552972555.jpg', '2019-03-19 12:15:55'),
(5, 'photo_PAaNcwsBKtpL_1552972938.jpg', '2019-03-19 12:22:18'),
(6, 'photo_MmzPnAVTXEUe_1552972974.jpg', '2019-03-19 12:22:54'),
(7, 'photo_UeF5dPRm6Iko_1552976782.jpg', '2019-03-19 13:26:22'),
(8, 'photo_k9wmAnRO8E4K_1552976824.jpg', '2019-03-19 13:27:04'),
(12, 'photo_zYWSMHQmbBTg_1552977772.jpg', '2019-03-19 13:42:52'),
(13, 'photo_xHWt2DVSO6rb_1553045713.jpg', '2019-03-20 08:35:13'),
(14, 'photo_YDc8aRHOuxIK_1553046276.jpg', '2019-03-20 08:44:36'),
(15, 'photo_rXwtF5xRSAZk_1553106647.jpg', '2019-03-21 01:30:47'),
(18, 'photo_tlaQLVZz6yPw_1559395630.jpg', '2019-06-01 20:27:10'),
(19, 'photo_kpS4AcjtnXar_1561793981.jpg', '2019-06-29 14:39:41'),
(20, 'photo_soBqNldH_ey8_1561794925.jpg', '2019-06-29 14:55:25'),
(23, 'photo_oqsINzMLVWR_1561929809.jpg', '2019-07-01 04:23:29'),
(24, 'photo_YNDfxcSz1jqO_1561930063.jpg', '2019-07-01 04:27:43'),
(25, 'photo_AS4hkV2r7xvq_1561930137.jpg', '2019-07-01 04:28:57'),
(26, 'photo_cu7yZ5hKUBzw_1561930228.jpg', '2019-07-01 04:30:28'),
(27, 'photo_eFKIcCpAOdB7_1561930286.jpg', '2019-07-01 04:31:26'),
(28, 'photo_fXIQaCSMbcEt_1562267778.gif', '2019-07-05 02:16:18'),
(29, 'photo_gLUkHS4cuzfG_1562267926.gif', '2019-07-05 02:18:46');

-- --------------------------------------------------------

--
-- Table structure for table `internal_user`
--

CREATE TABLE `internal_user` (
  `id` int(11) NOT NULL,
  `username` varchar(35) NOT NULL,
  `password` text NOT NULL,
  `kode_internal` varchar(9) NOT NULL COMMENT 'kode_internal juga berfungsi sebagai kode_reveral',
  `kode_mirror_internal` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `level_user` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  `kode_rekening` varchar(25) NOT NULL,
  `bank` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internal_user`
--

INSERT INTO `internal_user` (`id`, `username`, `password`, `kode_internal`, `kode_mirror_internal`, `status`, `level_user`, `last_login`, `kode_rekening`, `bank`) VALUES
(1, 'admin', 'WWxkV2RWb3pWblZhUjBaMVdqSkdkVnBIUmpGak1sWjVZVEpXZFdSSVZuVmFla2t6VGxSck5VMUJQVDA9', 'admin', 'admin', 1, 1, '2019-07-05 05:01:14', '', ''),
(2, 'customer', 'WWxkV2RWb3pWblZhUjBaMVdqSkdkVnBIUmpGak1sWjVZVEpXZFdSSVZuVmFla2t6VGxSck5VMUJQVDA9', 'customer', 'customer', 1, 3, '2019-07-11 15:30:48', '', ''),
(4, 'web', 'WWxkV2RWb3pWblZhUjBaMVdqSkdkVnBIUmpGak1sWjVZVEpXZFdSSVZuVmFla2t6VGxSck5VMUJQVDA9', 'AX123456', 'AX123456', 1, 2, '2019-07-11 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `internal_user_level`
--

CREATE TABLE `internal_user_level` (
  `id` int(11) NOT NULL,
  `nama` varchar(16) NOT NULL,
  `description` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internal_user_level`
--

INSERT INTO `internal_user_level` (`id`, `nama`, `description`) VALUES
(1, 'Super Admin', 'All Access Menu'),
(2, 'Trouble Shooting', 'Some Menu'),
(3, 'Customer', 'Only Update Content');

-- --------------------------------------------------------

--
-- Table structure for table `is_page`
--

CREATE TABLE `is_page` (
  `id` int(11) NOT NULL,
  `image` varchar(225) NOT NULL,
  `head_title` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `kode_internal` varchar(9) NOT NULL,
  `page_name` varchar(15) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `is_page`
--

INSERT INTO `is_page` (`id`, `image`, `head_title`, `title`, `content`, `kode_internal`, `page_name`, `status`) VALUES
(1, '', 'Our Wedding', 'Juni 15, 2019 SIDOARJO', '<p>We invited you to celebrate our wedding</p>\r\n', '', 'PAGE1', 1),
(2, 'photo_OagCVI3A5Elb_1559395664.jpg', 'OUR SPECIAL EVENTS', 'Wedding Events', '<p>We invited you to celebrate our wedding</p>\r\n', '', 'PAGE2', 1),
(3, '', 'WE LOVE EACH OTHER', 'OUR STORY', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.', '', 'PAGE3', 1),
(4, '', 'OUR MEMORIES', 'Wedding Gallery', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.', '', 'PAGE4', 1),
(5, 'photo_OIprHy2NhZqi_1559377872.jpg', '', '', '<p style=\"text-align:center\"><span style=\"font-size:26px\">ٱلْخَبِيثَٰتُ لِلْخَبِيثِينَ وَٱلْخَبِيثُونَ لِلْخَبِيثَٰتِ ۖ وَٱلطَّيِّبَٰتُ لِلطَّيِّبِينَ وَٱلطَّيِّبُونَ لِلطَّيِّبَٰتِ ۚ أُو۟لَٰٓئِكَ مُبَرَّءُونَ مِمَّا</span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:26px\">َقُولُونَ ۖ لَهُم مَّغْفِرَةٌۭ وَرِزْقٌۭ كَرِيمٌۭ</span></p>\r\n\r\n<p>Wanita yang baik adalah untuk lelaki yang baik. Lelaki yang baik untuk wanita yang&nbsp;<br />\r\nbaik pula (begitu pula sebaliknya). Bagi mereka ampunan dan reski yang melimpah&nbsp;<br />\r\n(yaitu:Surga)&rdquo; [QS. An Nuur (24):26].</p>\r\n', '', 'PAGE5', 1),
(6, '', 'Kartu Ucapan', 'Teman, Kerabat & Tamu Undangan', 'Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.', '', 'PAGE6', 1),
(7, 'photo_TvigREGrYAdp_1559392840.jpg', '', 'Terima Kasih', '<p>Please Fill-up the form to notify you that you</p>\r\n', '', 'PAGE7', 1);

-- --------------------------------------------------------

--
-- Table structure for table `konfiguration`
--

CREATE TABLE `konfiguration` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `paket` varchar(10) NOT NULL,
  `is_template` varchar(10) NOT NULL,
  `bg` varchar(225) NOT NULL,
  `mempelai_pria` varchar(225) NOT NULL,
  `photo_pria` varchar(225) NOT NULL,
  `mempelai_wanita` varchar(225) NOT NULL,
  `photo_wanita` varchar(225) NOT NULL,
  `ayah_pria` varchar(55) NOT NULL,
  `ibu_pria` varchar(55) NOT NULL,
  `ayah_wanita` varchar(55) NOT NULL,
  `ibu_wanita` varchar(55) NOT NULL,
  `content_pria` text NOT NULL,
  `content_wanita` text NOT NULL,
  `acara_resepsi` date NOT NULL,
  `start_at` time NOT NULL,
  `end_at` time NOT NULL,
  `email` varchar(225) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `whatsapp` varchar(15) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `kecamatan` int(11) NOT NULL,
  `kota` int(11) NOT NULL,
  `latitude` varchar(55) NOT NULL,
  `longtitude` varchar(55) NOT NULL,
  `kode_internal` varchar(9) NOT NULL,
  `status` int(11) NOT NULL,
  `expired_date` datetime NOT NULL,
  `author_is` varchar(350) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfiguration`
--

INSERT INTO `konfiguration` (`id`, `id_user`, `paket`, `is_template`, `bg`, `mempelai_pria`, `photo_pria`, `mempelai_wanita`, `photo_wanita`, `ayah_pria`, `ibu_pria`, `ayah_wanita`, `ibu_wanita`, `content_pria`, `content_wanita`, `acara_resepsi`, `start_at`, `end_at`, `email`, `telp`, `whatsapp`, `alamat`, `kecamatan`, `kota`, `latitude`, `longtitude`, `kode_internal`, `status`, `expired_date`, `author_is`, `created_at`) VALUES
(1, 0, 'SILVER', '', 'photo_eCpTzGt2v93W_1561797559.jpg', 'Bagus', 'photo_puF4aMYszfAE_1553073094.jpg', 'Melinda', 'photo_JZX7rDy5tYIi_1558838351.jpg', 'Paimin. MH, Sh', 'JJ', 'Suyanto', 'Nuraini', 'Manager IT PT. BIMA BANGUN SEJAHTERA\r\n', 'Enterpreuner', '2019-06-15', '19:00:00', '22:00:00', 'Gatot@gmail.com', '0854976852', '0854976852', 'Desa Modong RT 3 RW 2 Kec. Tulangan SIDOARJO', 0, 0, '', '', 'admin', 0, '2019-07-06 07:17:19', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `orders_undangan`
--

CREATE TABLE `orders_undangan` (
  `id` int(11) NOT NULL,
  `kode_pesanan` varchar(20) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_link` varchar(50) NOT NULL,
  `mempelai_pria` varchar(200) NOT NULL,
  `mempelai_wanita` varchar(200) NOT NULL,
  `ayah_pria` varchar(200) NOT NULL,
  `ibu_pria` varchar(200) NOT NULL,
  `ayah_wanita` varchar(200) NOT NULL,
  `ibu_wanita` varchar(200) NOT NULL,
  `acara_resepsi` date NOT NULL,
  `start_at` time NOT NULL,
  `end_at` time NOT NULL,
  `telp` varchar(13) NOT NULL,
  `whatsapp` varchar(13) NOT NULL,
  `email` varchar(25) NOT NULL,
  `paket` varchar(10) NOT NULL,
  `is_template` varchar(10) NOT NULL,
  `jenis_acara` varchar(15) NOT NULL,
  `reveral_from` varchar(10) NOT NULL,
  `komisi_reveral` int(11) NOT NULL COMMENT 'pembagian komisi untuk yang reveral atau yang promosikan',
  `status` varchar(12) NOT NULL COMMENT 'order,payment,confirm,expired,lewat_batas_pembayaran',
  `pesan` text NOT NULL,
  `created_at` int(11) NOT NULL,
  `transaksi_upload` datetime NOT NULL,
  `confirm_at` datetime NOT NULL,
  `confirm_from` varchar(25) NOT NULL,
  `batas_transfer_pembayaran` datetime NOT NULL,
  `status_komisi_reveral` varchar(10) NOT NULL COMMENT 'pending,proses_bayar,sudah_terbayar,menunggu_pembayaran',
  `transfer_komisi_reveral` datetime NOT NULL,
  `pesan_untuk_reveral` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `our_story`
--

CREATE TABLE `our_story` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `history_date` date NOT NULL,
  `tampilkan_date_history` int(11) NOT NULL,
  `content` text NOT NULL,
  `urutan` int(11) NOT NULL,
  `kode_internal` varchar(9) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `our_story`
--

INSERT INTO `our_story` (`id`, `title`, `image`, `history_date`, `tampilkan_date_history`, `content`, `urutan`, `kode_internal`, `status`) VALUES
(1, 'First We Meet', 'photo_PVIQH7g6nmYA_1553106769.jpg', '2019-03-21', 0, 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. ', 1, '', 1),
(2, 'First Date', 'photo_JvuVKl_Y8Utz_1553107118.jpg', '2019-03-31', 1, 'Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.', 2, '', 1),
(3, 'In A Relationship', 'photo_u2qdIDvBhPfJ_1553107256.jpg', '2019-06-01', 1, 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.', 3, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rekening_q`
--

CREATE TABLE `rekening_q` (
  `id` int(11) NOT NULL,
  `nama_bank` varchar(10) NOT NULL,
  `atas_nama` varchar(55) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `kode_rekening` varchar(25) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `photo` varchar(225) NOT NULL,
  `reviews` varchar(450) NOT NULL,
  `kode_internal` varchar(9) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `image` varchar(150) NOT NULL,
  `title` varchar(225) NOT NULL,
  `subtitle` varchar(350) NOT NULL,
  `subtitle_more` varchar(225) NOT NULL,
  `kode_internal` varchar(9) NOT NULL,
  `created_at` datetime NOT NULL,
  `urut` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `update_from` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `image`, `title`, `subtitle`, `subtitle_more`, `kode_internal`, `created_at`, `urut`, `status`, `last_update`, `update_from`) VALUES
(1, 'photo_cQqAI3wk0Blh_1561928910.jpg', '', '', '', 'admin', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `wedding_event`
--

CREATE TABLE `wedding_event` (
  `id` int(11) NOT NULL,
  `event_date` date NOT NULL,
  `start_at` time NOT NULL,
  `end_at` time NOT NULL,
  `title` varchar(225) NOT NULL,
  `description` varchar(450) NOT NULL,
  `kode_internal` varchar(9) NOT NULL,
  `user_action` varchar(25) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wedding_event`
--

INSERT INTO `wedding_event` (`id`, `event_date`, `start_at`, `end_at`, `title`, `description`, `kode_internal`, `user_action`, `last_update`) VALUES
(1, '2019-03-14', '08:00:00', '13:00:00', 'AKAD PERNIKAHAN', 'Insya allah akan di selenggarakan di kediaman mempelai wanita', 'admin', '', '0000-00-00 00:00:00'),
(3, '2019-03-15', '10:00:00', '23:00:00', 'TERIMA TAMU', 'Insya allah akan di selenggarakan di kediaman mempelai wanita', 'admin', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wedding_invitation`
--

CREATE TABLE `wedding_invitation` (
  `id` int(11) NOT NULL,
  `photo` varchar(225) NOT NULL,
  `nama` varchar(155) NOT NULL,
  `email` varchar(55) NOT NULL,
  `telp` varchar(18) NOT NULL,
  `address` varchar(200) NOT NULL,
  `note` text NOT NULL,
  `is_come` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `galleri`
--
ALTER TABLE `galleri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_temp`
--
ALTER TABLE `image_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internal_user`
--
ALTER TABLE `internal_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internal_user_level`
--
ALTER TABLE `internal_user_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `is_page`
--
ALTER TABLE `is_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `konfiguration`
--
ALTER TABLE `konfiguration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_undangan`
--
ALTER TABLE `orders_undangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_story`
--
ALTER TABLE `our_story`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rekening_q`
--
ALTER TABLE `rekening_q`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wedding_event`
--
ALTER TABLE `wedding_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wedding_invitation`
--
ALTER TABLE `wedding_invitation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `galleri`
--
ALTER TABLE `galleri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `image_temp`
--
ALTER TABLE `image_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `internal_user`
--
ALTER TABLE `internal_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `internal_user_level`
--
ALTER TABLE `internal_user_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `is_page`
--
ALTER TABLE `is_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `konfiguration`
--
ALTER TABLE `konfiguration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders_undangan`
--
ALTER TABLE `orders_undangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `our_story`
--
ALTER TABLE `our_story`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rekening_q`
--
ALTER TABLE `rekening_q`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wedding_event`
--
ALTER TABLE `wedding_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wedding_invitation`
--
ALTER TABLE `wedding_invitation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
