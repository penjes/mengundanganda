
        <section section-scroll="3" class="event-area section2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="section_heading">
                            <p>Most awaited moment of our life</p>
                            <h2>The Wedding Events</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="events line">
                            <h2>Main Ceremony</h2>
                            <div class="con">
                                <ul>
                                    <li><i class="fas fa-calendar-alt"></i><span>10 june 2018</span></li>
                                    <li><i class="fas fa-clock"></i><span> 10:00 AM to 12:00 PM</span></li>
                                    <li><i class="fas fa-map-marker-alt"></i><span>3355, Wayside Lane,<br>PIcland, CA 94601</span></li>
                                </ul>
                            </div>
                            <a href="#"><span><i class="fas fa-map-marker-alt"></i>Open Map</span></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-4">
                        <div class="events event_half line">
                            <h2>Wedding Party</h2>
                            <div class="con">
                                <ul>
                                    <li><i class="fas fa-calendar-alt"></i><span>10 june 2018</span></li>
                                    <li><i class="fas fa-clock"></i><span> 10:00 AM to 12:00 PM</span></li>
                                    <li><i class="fas fa-map-marker-alt"></i><span>3355, Wayside Lane,<br>PIcland, CA 94601</span></li>
                                </ul>
                            </div>
                            <a href="#"><span><i class="fas fa-map-marker-alt"></i>Open Map</span></a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="events">
                            <h2>Dinner Party</h2>
                            <div class="con">
                                <ul>
                                    <li><i class="fas fa-calendar-alt"></i><span>10 june 2018</span></li>
                                    <li><i class="fas fa-clock"></i><span> 10:00 AM to 12:00 PM</span></li>
                                    <li><i class="fas fa-map-marker-alt"></i><span>3355, Wayside Lane,<br>PIcland, CA 94601</span></li>
                                </ul>
                            </div>
                            <a href="#"><span><i class="fas fa-map-marker-alt"></i>Open Map</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>