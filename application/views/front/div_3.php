<section section-scroll="2" class="love-story section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="section_heading">
                            <p>Joahn & Jaquline</p>
                            <h2>Our True love story</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="love-storys">
                            <div class="story-list">
                                <div class="col-md-6 col-sm-12 pd-0">
                                    <div class="story lefts">
                                        <div class="storys">
                                            <div class="top-fig">
                                                <figure><img src="images/story1.jpg" alt=""/></figure>
                                                <div class="con">
                                                    <p>12 jan 2018</p>
                                                    <h2>How to meet</h2>
                                                </div>
                                            </div>
                                            <div class="content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="story lefts">
                                        <div class="storys">
                                            <div class="top-fig">
                                                <figure><img src="images/story1.jpg" alt=""/></figure>
                                                <div class="con">
                                                    <p>12 jan 2018</p>
                                                    <h2>How to meet</h2>
                                                </div>
                                            </div>
                                            <div class="content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="story lefts">
                                        <div class="storys">
                                            <div class="top-fig">
                                                <figure><img src="images/story1.jpg" alt=""/></figure>
                                                <div class="con">
                                                    <p>12 jan 2018</p>
                                                    <h2>How to meet</h2>
                                                </div>
                                            </div>
                                            <div class="content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 pd-0">
                                    <div class="story rights first">
                                        <div class="storys">
                                            <div class="top-fig">
                                                <figure><img src="images/story1.jpg" alt=""/></figure>
                                                <div class="con">
                                                    <p>12 jan 2018</p>
                                                    <h2>How to meet</h2>
                                                </div>
                                            </div>
                                            <div class="content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="story rights">
                                        <div class="storys">
                                            <div class="top-fig">
                                                <figure><img src="images/story1.jpg" alt=""/></figure>
                                                <div class="con">
                                                    <p>12 jan 2018</p>
                                                    <h2>How to meet</h2>
                                                </div>
                                            </div>
                                            <div class="content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        