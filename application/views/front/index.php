	<div id="video" class="video-bg iq-bg iq-bg-fixed iq-box-shadow iq-over-black-80" data-vide-bg="<?=base_url()?>assets/frontend/video/01" data-vide-options="position: 0% 50%" style="width: 100%; height:100%;">
		<section id="iq-home" class="banner-03">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1 class="iq-font-white iq-tw-8 text-center">Undangan Digital
							<small class="iq-font-white iq-tw-6">
								<b class="iq-font-white">&#9679;</b> Pernikahan 
								<b class="iq-font-white">&#9679;</b> Reuni Sekolah 
								<b class="iq-font-white">&#9679;</b> Ulang Tahun
								<b class="iq-font-white">&#9679;</b> Dan lain-lain
							</small>
						</h1>
						<div class="iq-mobile-app text-center">
							<div class="iq-mobile-box">
								<img class="iq-mobile-img" src="<?=base_url()?>assets/frontend/images/banner/mengundanganda.png" alt="#">
								<span data-depth="0.8" class="layer iq-mobile-icon icon-01" data-bottom="transform:translateX(50px)" data-top="transform:translateX(-100px);">
									<img src="<?=base_url()?>assets/frontend/images/banner/04.png" alt="#">
								</span>
								<span data-depth="0.8" class="layer iq-mobile-icon icon-02" data-bottom="transform:translateX(100px)" data-top="transform:translateX(0px);">
									<img src="<?=base_url()?>assets/frontend/images/banner/05.png" alt="#">
								</span>
								<span data-depth="0.8" class="layer iq-mobile-icon icon-03" data-bottom="transform:translateX(-50px)" data-top="transform:translateX(40px);">
									<img src="<?=base_url()?>assets/frontend/images/banner/06.png" alt="#">
								</span>
								<span data-depth="0.8" class="layer iq-mobile-icon icon-04" data-bottom="transform:translateX(-30px)" data-top="transform:translateX(0px);">
									<img src="<?=base_url()?>assets/frontend/images/banner/07.png" alt="#">
								</span>
								<span data-depth="0.8" class="layer iq-mobile-icon icon-05" data-bottom="transform:translateX(0px)" data-top="transform:translateX(-50px);">
									<img src="<?=base_url()?>assets/frontend/images/banner/08.png" alt="#">
								</span>
							</div>
						</div>
			<!-- 			<div class="link">
							<h5 class="iq-font-white" data-animation="animated fadeInLeft">Free Download Here</h5>
							<ul class="list-inline" data-animation="animated fadeInUp">
								<li><a href="javascript:void(0)"><i class="ion-social-apple"></i></a></li>
								<li><a href="javascript:void(0)"><i class="ion-social-android"></i></a></li>
								<li><a href="javascript:void(0)"><i class="ion-social-windows"></i></a></li>
							</ul>
							<p class="iq-font-white iq-mt-10">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.</p>
						</div> -->
					</div>
				</div>
			</div>
		</section>
	</div>


	<section id="about-us" class="overview-block-ptb iq-about-1 grey-bg">
		<div class="container">
			<div class="row iq-mt-20">
				<div class="col-sm-12 col-md-6 col-lg-6 iq-mt-30 popup-gallery">
					<h2 class="heading-left iq-tw-6 iq-mt-40">Kenapa harus beralih ke Undangan Digital??</h2>
					<div class="lead iq-tw-7 iq-mb-20">
						Undangan menggunakan kertas sudah sangat ketinggalan jaman, selain itu setelah d pakai pasti berakhir ditempat sampah, harganya mahal untuk tamu undangan yang sangat banyak, dan pastinya kurang menarik.
					</div>
					<p>
						Kini hadir undangan digital yang ramah lingkungan karena tidak memakai kertas, hemat di kantong karena harga mudah dijangkau untuk jumlah tamu undangan yang tidak terbatas, dan yang pasti kekinian.<br>Anda tidak perlu membagikan kerumah-rumah temen anda atau titipin undangan ke temen anda, anda hanya perlu bagikan link website yang bisa custom nama anda sendiri.
						Simple kan?? tidak perlu merepotkan temen anda, tidak perlu mahal-mahal untuk budget undangan dan yang pasti undangan anda sangat menarik.
						<br><br>Ada keuntungan jika anda mengajak temen anda menggunakan jasa kami, anda akan mendapatkan 10% dari total transaksi. asalkan teman anda menggunakan kode referal anda dan berhasil melakukan transaksi. Bonus akan kami transfer kenomer rekening dalam waktu 48jam setelah transaksi berhasil.
						<br>Pelayanan kami 24 jam, jadi jika anda perlu bantuan anda bisa hubungi kami. 
					</p>
				<!-- 	<div class="btn-group iq-mt-40" role="group" aria-label="">
						<a class="button popup-youtube" href="video/01.mp4">Watch Video Demo</a>
					</div> -->
				</div>
				<div class="col-sm-12 col-md-6 col-lg-6">
					<img class="img-responsive center-block" src="<?=base_url()?>assets/frontend/images/sample.png" alt="#">
				</div>
			</div>
		</div>
	</section>


	<section id="features" class="overview-block-ptb iq-amazing-tab white-bg">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="heading-title">
						<h2 class="title iq-tw-6">Fitur Spesial</h2>
						<div class="divider"></div>
						<!-- <p>Appino is launch with everything you need. We've got a lot of amaing and cool features. so here we go, with unlimited features. go and check out!</p> -->
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-4">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active wow fadeInLeft" data-wow-duration="1s">
							<a class="round-right" href="#design" aria-controls="design" role="tab" data-toggle="tab">
								<div class="iq-fancy-box-01 text-right">
									<i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
									<h4 class="iq-tw-6">Responsive Design</h4>
									<div class="fancy-content-01">
										<p>Design yang menarik dibuat untuk tampilan dekstop ataupun mobile</p>
									</div>
								</div>
							</a>
						</li>
						<li role="presentation" class="wow fadeInLeft" data-wow-duration="1s">
							<a class="round-right" href="#resolution" aria-controls="resolution" role="tab" data-toggle="tab">
								<div class="iq-fancy-box-01 text-right">
									<i aria-hidden="true" class="ion-ios-color-wand-outline"></i>
									<h4 class="iq-tw-6">Backend System</h4>
									<div class="fancy-content-01">
										<p>Informasi Acara diinputkan melalui backend System, mempermudah anda untuk merubah informasi didalam undangan Digital </p>
									</div>
								</div>
							</a>
						</li>
						<li role="presentation" class="wow fadeInLeft" data-wow-duration="1s">
							<a class="round-right" href="#ready" aria-controls="ready" role="tab" data-toggle="tab">
								<div class="iq-fancy-box-01 text-right">
									<i aria-hidden="true" class="ion-ios-copy-outline"></i>
									<h4 class="iq-tw-6">Galery Foto</h4>
									<div class="fancy-content-01">
										<p>Tersedia Layout untuk menampilkan momen-momen terbaik anda sehingga dapat dilihat oleh banyak orang</p>
									</div>
								</div>
							</a>
						</li>
					</ul>
				</div>
				<div class="col-md-4 text-center hidden-sm hidden-xs">

					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="design">
							<img src="<?=base_url()?>assets/frontend/images/screenshots/mengundanganda.png" class="img-responsive center-block" alt="#">
						</div>
						<div role="tabpanel" class="tab-pane" id="resolution">
							<img src="<?=base_url()?>assets/frontend/images/screenshots/fitur-backend.jpg" class="img-responsive center-block" alt="#">
						</div>
						<div role="tabpanel" class="tab-pane" id="ready">
							<img src="<?=base_url()?>assets/frontend/images/screenshots/fitur-galery.jpg" class="img-responsive center-block" alt="#">
						</div>
						<div role="tabpanel" class="tab-pane" id="fertures">
							<img src="<?=base_url()?>assets/frontend/images/screenshots/04.jpg" class="img-responsive center-block" alt="#">
						</div>
						<div role="tabpanel" class="tab-pane" id="face">
							<img src="<?=base_url()?>assets/frontend/images/screenshots/fitur-map.jpg" class="img-responsive center-block" alt="#">
						</div>
						<div role="tabpanel" class="tab-pane" id="codes">
							<img src="<?=base_url()?>assets/frontend/images/screenshots/06.jpg" class="img-responsive center-block" alt="#">
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-4">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="wow fadeInRight" data-wow-duration="1s">
							<a href="#fertures" aria-controls="fertures" role="tab" data-toggle="tab">
								<div class="iq-fancy-box-01">
									<i aria-hidden="true" class="ion-ios-photos-outline"></i>
									<h4 class="iq-tw-6">Tanpa batasan jumlah</h4>
									<div class="fancy-content-01">
										<p>Hanya bayar 1x, anda bebas sebar linnk undangan anda tanpa batasan jumlah.</p>
									</div>
								</div>
							</a>
						</li>
						<li role="presentation" class="wow fadeInRight" data-wow-duration="1s">
							<a href="#face" aria-controls="face" role="tab" data-toggle="tab">
								<div class="iq-fancy-box-01">
									<i aria-hidden="true" class="ion-ios-heart-outline"></i>
									<h4 class="iq-tw-6">Google Maps</h4>
									<div class="fancy-content-01">
										<p>Tersedia Google maps yang dapat menunjukan arah kepada tamu anda ke lokasi acara dengan 1x klik.</p>
									</div>
								</div>
							</a>
						</li>
						<li role="presentation" class="wow fadeInRight" data-wow-duration="1s">
							<a href="#codes" aria-controls="codes" role="tab" data-toggle="tab">
								<div class="iq-fancy-box-01">
									<i aria-hidden="true" class="ion-ios-plus-outline"></i>
									<h4 class="iq-tw-6">Dan banyak lagi fitur-fitur lainnya</h4>
									<div class="fancy-content-01">
										<p>Custom Domain, Buku Undangan, Nama Penerima, Pesan Kehadiran Tamu Undangan</p>
									</div>
								</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section id="pricing" class="overview-block-ptb grey-bg iq-price-table">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="heading-title">
						<h2 class="title iq-tw-6">Penawaran Harga</h2>
						<div class="divider"></div>
						<p>Terdapat kode referal yang anda bisa bagikan, dan dapatkan 10% dari setiap transaksi yang berhasil dengan kode referal anda.</p>
					</div>
				</div>
			</div>
			<div class="row"> 
				
				<div class="col-sm-6 col-md-4 re7-mt-50">
					<div class="iq-pricing text-center">
						<div class="price-title green-bg ">
							<h1 class="iq-font-white iq-tw-7"><small>IDR</small>74k<small>/events</small></h1>
							<span class="text-uppercase iq-tw-6 iq-font-white">STANDARD</span>
						</div>
						<ul>
							<li>1 month launching</li>
							<li>no Galery images</li>
							<li>Free tempalate</li>
							<li>no Domain custome</li>
							<li>free attendance list</li>
							<li>Backend System</li>
						</ul>
						<div class="price-footer">
							<a class="button" href="<?= base_url()?>order/standart ">Purchase</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 re-mt-30 wow flipInY" data-wow-duration="1s">
					<div class="iq-pricing text-center">
						<div class="price-title green-bg ">
							<h1 class="iq-font-white iq-tw-7"><small>IDR</small>150k<small>/events</small></h1>
							<span class="text-uppercase iq-tw-6 iq-font-white">Premium</span>
						</div>
						<ul>
							<li>1 month launching</li>
							<li>Galeri limit 7 image</li>
							<li>Free tempalate</li>
							<li>no Domain custome</li>
							<li>free attendance list</li>
							<li>Backend System</li>
						</ul>
						<div class="price-footer">
							<a class="button" href="<?= base_url()?>order/premium ">Purchase</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 re-mt-30">
					<div class="iq-pricing text-center">
						<div class="price-title green-bg ">
							<h1 class="iq-font-white iq-tw-7"><small>IDR</small>300k<small>/events</small></h1>
							<span class="text-uppercase iq-tw-6 iq-font-white">UNLIMITED</span>
						</div>
						<ul>
							<li>1 month launching</li>
							<li>Galeri limit 10 image</li>
							<li>Free tempalate</li>
							<li>Domain custome</li>
							<li>free attendance list</li>
							<li>Backend System</li>
						</ul>
						<div class="price-footer">
							<a class="button" href="<?= base_url()?>order/ultimate ">Purchase</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="screenshots" class="iq-app iq-bg iq-bg-fixed iq-font-white" style="background: url('<?=base_url()?>assets/frontend/images/banner/bg2.jpg');">
		<div class="container-fluid">
			<div class="row row-eq-height">
				<div class="col-md-6 text-left iq-ptb-80 green-bg">
					<div class="iq-app-info">
						<h2 class="heading-left iq-font-white white iq-tw-6 ">Template Undangan</h2>
						<div class="lead iq-tw-6 iq-mb-20">Template Undangan yang simple dan menarik yang bisa anda pilih</div>
						<h4 class="iq-mt-50 iq-font-white iq-tw-6 iq-mb-15">mengundanganda.com</h4>
						<p class="">menyediakan template yang cocok untuk acara pernikahan, reuni sekolah, ulang tahun, dan lain-lain.</p>
					</div>
				</div>
				<div class="col-md-6 iq-app-screen iq-ptb-80">
					<div class="home-screen-slide">
						<div class="owl-carousel popup-gallery" data-autoplay="true" data-loop="true" data-nav="false" data-dots="false" data-items="3" data-items-laptop="2" data-items-tab="2" data-items-mobile="1" data-items-mobile-sm="1" data-margin="15">
							<?php for($i=1;$i<=6;$i++){ ?>
							<div class="item"><a href="images/screenshots/tema-<?=$i?>.jpg" class="popup-img">
								<img class="img-responsive" src="<?=base_url()?>assets/frontend/images/screenshots/tema-<?=$i?>.jpg" alt="#"></a>
							</div>
							<?php } ?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="overview-block-ptb grey-bg iq-loved-customers">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="heading-title">
						<h2 class="title iq-tw-6">Apa Kata Mereka Tentang Kami ?</h2>
						<div class="divider"></div>
						<!-- <p>Our Customer are our priority. we believe in providing best services to them. </p> -->
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="true" data-dots="false" data-items="3" data-items-laptop="2" data-items-tab="2" data-items-mobile="1" data-items-mobile-sm="1" data-margin="15">
						<div class="item">
							<div class="iq-client white-bg">
								<div class="client-img">
									<img alt="#" class="img-responsive img-circle" src="<?=base_url()?>assets/frontend/images/testimonial/01.jpg">
								</div>
								<div class="client-info">
									<div class="client-name iq-mb-10">
										<h5 class="iq-tw-6">Jason Adams</h5>
										<span class="sub-title iq-tw-6">CEO, Appino</span>
									</div>
									<p>Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incididunt ut labore.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="iq-client white-bg">
								<div class="client-img">
									<img alt="#" class="img-responsive img-circle" src="<?=base_url()?>assets/frontend/images/testimonial/02.jpg">
								</div>
								<div class="client-info">
									<div class="client-name iq-mb-10">
										<h5 class="iq-tw-6">Amy Adams</h5>
										<span class="sub-title iq-tw-6">CEO, Appino</span>
									</div>
									<p>Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incididunt ut labore.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="iq-client white-bg">
								<div class="client-img">
									<img alt="#" class="img-responsive img-circle" src="<?=base_url()?>assets/frontend/images/testimonial/03.jpg">
								</div>
								<div class="client-info">
									<div class="client-name iq-mb-10">
										<h5 class="iq-tw-6">John Deo</h5>
										<span class="sub-title iq-tw-6">CEO, Appino</span>
									</div>
									<p>Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incididunt ut labore.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>



	<section id="contact-us" class="iq-full-contact white-bg">
		<div class="container-fluid">
			<div class="row no-gutter">
				<div class="col-sm-6">
					<div class="iq-map">
						<iframe width="100%" height="600" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=-7.315306197903035, 112.72659301757814&amp;q=+(Mengundanganda.com)&amp;ie=UTF8&amp;t=&amp;z=10&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/map-my-route/">Create a route on google maps</a></iframe>
					</div>
				</div>
				<div class="col-sm-6 iq-mt-20 iq-pall-40">
					<h4 class="heading-left iq-tw-6 iq-pb-20">Hubungi Kami</h4>
					<div class="row">
						<div id="formmessage">Success/Error Message Goes Here</div>
						<form class="form-horizontal" id="contactform" method="post" action="https://iqonicthemes.com/themes/appino/video-background/php/contact-form.php">
							<div class="contact-form">
								<div class="col-sm-6">
									<div class="section-field">
										<input id="name" type="text" placeholder="Name*" name="name">
									</div>
									<div class="section-field">
										<input type="email" placeholder="Email*" name="email">
									</div>
									<div class="section-field">
										<input type="text" placeholder="Phone*" name="phone">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="section-field textarea">
										<textarea class="input-message" placeholder="Comment*" rows="7" name="message"></textarea>
									</div>
									<input type="hidden" name="action" value="sendEmail" />
									<button id="submit" name="submit" type="submit" value="Send" class="button pull-right iq-mt-40">Send Message</button>
								</div>
							</div>
						</form>
						<div id="ajaxloader" style="display:none"><img class="center-block mt-30 mb-30" src="<?=base_url()?>assets/frontend/images/ajax-loader.gif" alt=""></div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row iq-ptb-80">
				<div class="col-sm-6 col-md-4 col-lg-4">
					<div class="iq-fancy-box-04">
						<div class="iq-icon green-bg">
							<i aria-hidden="true" class="ion-ios-location-outline"></i>
						</div>
						<div class="fancy-content">
							<h5 class="iq-tw-6">Address</h5>
							<span class="lead iq-tw-6">Surabaya, Jawa timur</span>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-4">
					<div class="iq-fancy-box-04">
						<div class="iq-icon green-bg">
							<i aria-hidden="true" class="ion-ios-telephone-outline"></i>
						</div>
						<div class="fancy-content">
							<h5 class="iq-tw-6">Phone</h5>
							<span class="lead iq-tw-6">083847754493</span>
							<p class="iq-mb-0">Everyday</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-4">
					<div class="iq-fancy-box-04">
						<div class="iq-icon green-bg">
							<i aria-hidden="true" class="ion-ios-email-outline"></i>
						</div>
						<div class="fancy-content">
							<h5 class="iq-tw-6">Mail</h5>
							<span class="lead iq-tw-6">
								<a href="https://iqonicthemes.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="bdd0dcd4d1fddccdcdd4d3d293ded2d0">[email&#160;protected]</a></span>
								<p class="iq-mb-0">24 X 7 online support</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

