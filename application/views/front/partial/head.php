<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--Theme Title-->
        <title><?php echo ucfirst($biodata->mempelai_pria);?> &amp; <?php echo ucfirst($biodata->mempelai_wanita);?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo ucfirst($biodata->mempelai_pria);?> &amp; <?php echo ucfirst($biodata->mempelai_wanita);?> mengundang anda" />
        <meta name="keywords" content="Datang di acara kami" />
        <meta name="author" content="mengundanganda.com" />


        <!-- Place favicon.ico in the root directory -->
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
        <link rel="apple-touch-icon" href="<?php echo base_url();?>images/favicon.png">
        
		<!-- All css Here -->
        <!-- All plugins css -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/allplugins.css">	
		
		<!-- Style css -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/style.css">
		<!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/responsive.css">

        <!-- Customization css -->
        <!--If u need any change then use this css file 
        <link rel="stylesheet" href="<?php echo base_url();?>assets/custom.css">

		<!-- Modernizr JavaScript -->
        <script src="<?php echo base_url();?>assets/modernizr-2.8.3.min.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>