<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from iqonicthemes.com/themes/appino/video-background/v3/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Jul 2019 04:26:56 GMT -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Appino - Responsive App Landing Page" />
	<meta name="author" content="iqonicthemes.in" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title>Undangan Digital | Undangan Kekinian</title>

	<link rel="shortcut icon" href="<?=base_url()?>assets/frontend/images/favicon.png" />

	<!-- <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;Raleway:300,400,500,600,700,800,900"> -->
	<link href='https://fonts.googleapis.com/css?family=Parisienne' rel='stylesheet'>

	<link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/frontend/css/owl-carousel/owl.carousel.css" />

	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/frontend/css/font-awesome.css" />

	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/frontend/css/magnific-popup/magnific-popup.css" />

	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/frontend/css/animate.css" />

	<link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/ionicons.min.css">

	<link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/style.css">

	<link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/responsive.css">
<!-- 
	<link rel="stylesheet" href="<?=base_url()?>assets/frontend/javascript:void(0)" data-style="styles"> -->
	<link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/style-customizer.css" />

	<link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/custom.css" />
</head>

<body>
	<style type="text/css">
			#loading-center>img{
				position: absolute;
				top: 50%;
				transform: translate(-50%);
				left: 50%;
				transform: translate(-50% ,-50%);
			}
		</style>

	<div id="loading" class="green-bg">
		<div id="loading-center">
			<img src="<?= base_url() ?>assets/frontend/images/logo1-white.png">

	</div>
</div>

<?php if(!empty($menu)){ ?>
<header id="header-wrap" class="affix">
<?php }else{ ?>
<header id="header-wrap" data-spy="affix" data-offset-top="55" >
<?php } ?>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<nav class="navbar navbar-default">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?=base_url()?>assets/frontend/javascript:void(0)">
							<img src="<?=base_url()?>assets/frontend/images/logo2-white.png" alt="logo">
						</a>
					</div>

					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right" id="top-menu">
							<li class="active"><a href="<?=base_url()?>">Home</a></li>
							<li><a href="<?=base_url()?>#about-us">About Us</a></li>
							<li><a href="<?=base_url()?>#features">Features</a></li>
							<li><a href="<?=base_url()?>#screenshots">Template</a></li>
							<li><a href="<?=base_url()?>#pricing">Pricing</a></li>				
							<li><a href="<?=base_url()?>#contact-us">Contact Us</a></li>
							<li><a href="<?=base_url()?>login">Login</a></li>
						</ul>
					</div>

				</nav>
			</div>
		</div>
	</div>
</header>
