<section section-scroll="0" class="slider-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 pd-0">
                        <div class="slider_home">
                            <?php 
                            $n=0;
                            $class = ['one','two','three'];
                            foreach ($slider as $key){ ?>
                            <div class="item <?php echo $class[$n];?>">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="content">
                                                <p class="wow animated fadeInDown" data-wow-duration="1.3s"><?php echo $key->title;?></p>
                                                <h2 class="wow animated fadeInDown" data-wow-duration="1s">
                                                 <?php echo ucfirst($biodata->mempelai_pria);?> 
                                                 <span>&</span> 
                                                 <?php echo ucfirst($biodata->mempelai_wanita);?></h2>
                                                <h3 class="wow animated fadeInUp" data-wow-duration="1s"><?php echo $biodata->tgl;?>/<?php echo $biodata->bln;?>/<?php echo $biodata->thn;?></h3><br>
                                                <span class="date wow animated fadeInUp" data-wow-duration="1.3s">Save the date</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $n++;} ?>
                            <!-- <div class="item two">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="content">
                                                 <p class="wow animated fadeInDown" data-wow-duration="1.3s">We Hope to See You Soon</p>
                                                <h2 class="wow animated fadeInDown" data-wow-duration="1s">Bagus <span>&</span> Nela</h2>
                                                <h3 class="wow animated fadeInUp" data-wow-duration="1s">02/02/2020</h3><br>
                                                <span class="date wow animated fadeInUp" data-wow-duration="1.3s">Save the date</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item three">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="content">
                                                  <p class="wow animated fadeInDown" data-wow-duration="1.3s">We Hope to See You Soon</p>
                                                <h2 class="wow animated fadeInDown" data-wow-duration="1s">Bagus <span>&</span> Nela</h2>
                                                <h3 class="wow animated fadeInUp" data-wow-duration="1s">02/02/2020</h3><br>
                                                <span class="date wow animated fadeInUp" data-wow-duration="1.3s">Save the date</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </section> 