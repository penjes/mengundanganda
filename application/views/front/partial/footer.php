
					<footer class="iq-footer-03 white-bg iq-ptb-20">
						<div class="container">
							<div class="row">
								<div class="col-sm-6">
									<div class="footer-copyright iq-pt-10">Copyright <span id="copyright"> 
										<!-- <script data-cfasync="false" src="../../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> -->
										<script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script></span> 
										<a href="javascript:void(0)" class="text-green">IT-Solution.</a> All Rights Reserved </div>
								</div>
								<div class="col-sm-6">
									<ul class="info-share">
										<li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
										<li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
										<li><a href="javascript:void(0)"><i class="fa fa-google"></i></a></li>
										<li><a href="javascript:void(0)"><i class="fa fa-github"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</footer>

				</div>

				<div id="back-to-top">
					<a class="top" id="top" href="#top"> <i class="ion-ios-upload-outline"></i> </a>
				</div>

<!-- 
				<div class="iq-customizer closed">
					<div class="buy-button"> <a class="opener" href="javascript:void(0)"><i class="fa fa-spinner fa-spin"></i></a> </div>
					<div class="clearfix content-chooser">
						<h3 class="iq-font-black">Appino Awesome Color</h3>
						<p>This color combo available inside whole template. You can change on your wish, Even you can create your own with limitless possibilities! </p>
						<ul class="iq-colorChange clearfix">
							<li class="color-1 selected" data-style="color-1"></li>
							<li class="color-2" data-style="color-2"></li>
							<li class="color-3" data-style="color-3"></li>
							<li class="color-4" data-style="color-4"></li>
							<li class="color-5" data-style="color-5"></li>
							<li class="color-6" data-style="color-6"></li>
							<li class="color-7" data-style="color-7"></li>
							<li class="color-8" data-style="color-8"></li>
						</ul>
						<a target="_blank" class="button" href="#">purchase now</a>
					</div>
				</div>
 -->

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/jquery.min.js"></script>

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/owl-carousel/owl.carousel.min.js"></script>

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/counter/jquery.countTo.js"></script>

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/jquery.appear.js"></script>

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/magnific-popup/jquery.magnific-popup.min.js"></script>

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/retina.min.js"></script>

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/skrollr.min.js"></script>

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/wow.min.js"></script>

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/jquery.countdown.min.js"></script>

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/jquery.vide.js"></script>

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/bootstrap.min.js"></script>

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/style-customizer.js"></script>

				<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/custom.js"></script>
			</body>

			
			</html>