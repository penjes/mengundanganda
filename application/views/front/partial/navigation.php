<header id="sticky">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-9">
                        <div class="logo-area">
                            <a href="index.html">
							<img src="/images/logo.png" alt=""/></a>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 hidden-xs">
                        <div class="main-menu menu_scroll">
                            <nav>
                                <ul>
                                    <li><a href="0">home</a></li>
                                    <li><a href="1">about us</a></li>
                                    <li><a href="2">Love Story</a></li>
                                    <li><a href="3">Events</a></li>
                                    <li><a href="4">Gallery</a></li>
                                    <li><a href="5">Blog</a></li>
                                    <li><a href="6">Contact Us</a></li>
                                    <li><a href="7" class="btn1">rspv</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
			<!--Responsive Menu area--> 
            <div class="mobilemenu">
                <div class="mobile-menu menu_scroll visible-xs">
                    <nav>
                        <ul>
                            <li><a href="0">home</a></li>
                            <li><a href="1">about us</a></li>
                            <li><a href="2">Love Story</a></li>
                            <li><a href="3">Events</a></li>
                            <li><a href="4">Gallery</a></li>
                            <li><a href="5">Blog</a></li>
                            <li><a href="6">Contact Us</a></li>
                            <li><a href="7">rspv</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!--Responsive Menu area End-->
        </header>