<section section-scroll="7" class="rspv-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="rspv-con">
                            <p>Are You Attending?</p>
                            <h2>Rsvp</h2>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 pd-0">
                        <div class="rspv-form">
                            <form>
                                <div class="col-sm-9 pd-0">
                                    <div class="col-sm-6">
                                        <fieldset>
                                            <label>Full Name </label>
                                            <input type="text" name="full_name" class="require">
                                        </fieldset>
                                    </div>
                                    <div class="col-sm-6">
                                        <fieldset>
                                            <label>Guest</label>
                                            <select name="guest_no" class="require">
                                                <option>01</option>
                                                <option>02</option>
                                                <option>03</option>
                                                <option>04</option>
                                            </select>
                                        </fieldset>
                                    </div>
                                    <div class="col-sm-6">
                                        <fieldset>
                                            <label>Email</label>
                                            <input type="email" name="email" class="require" data-valid="email" data-error="Email should be valid.">
                                        </fieldset>
                                    </div>
                                    <div class="col-sm-6">
                                        <fieldset>
                                            <label>All Events</label>
                                            <select name="event_name" class="require">
                                                <option>All Events</option>
                                                <option>Main Ceremony</option>
                                                <option>Wedding Party</option>
                                                <option>Dinner Party</option>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="response"></div>
							</div>
                                <div class="col-sm-3">
								<button type="button" class="submitForm" form-type="inquiry">I Am Attending</button>
                                   
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>