<section id="order" style="margin: 10% 0%">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<form>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<label class="input-group-text" for="inputGroupSelect01">Options</label>
						</div>
						<select class="custom-select" id="inputGroupSelect01">
							<option selected>Choose...</option>
							<option value="1">Standart</option>
							<option value="2">Premium</option>
							<option value="3">Unlimited</option>
						</select>
					</div>
					<div class="form-group">
						<label for="email">Email address:</label>
						<input type="email" class="form-control" id="email">
					</div>
					<div class="form-group">
						<label for="pwd">Tanggal Acara:</label>
						<input type="password" class="form-control" id="pwd">
					</div>
					<div class="form-group">
						<label for="pwd">Kode Referal</label>
						<input type="password" class="form-control" id="pwd">
					</div>
					<div class="checkbox">
						<label><input type="checkbox"> Remember me</label>
					</div>
					<button type="submit" class="btn btn-default">Order</button>
				</form>
			</div>
		</div>
	</div>
</section>