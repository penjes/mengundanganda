<section section-scroll="1" class="persons-area section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="section_heading">
                            <p>ARE GETTING MARRIED!</p>
                            <h2>Groom and Bride</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-sm-4">
                        <div class="persons">
                            <figure><img src="images/about1.png" alt=""/></figure>
                            <div class="content">
                                <a href="#"><img src="images/about1.png" alt=""/></a>
                                <h4>( S/o Mrs fariza & Mr. jany )</h4>
                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit connec.</p>
                                <ul class="socials">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-skype"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 pd-0">
                        <div class="persons-con">
                            <h1>Invitation</h1>
                            <span class="ico"><img src="images/icon5.png" alt=""/></span>
                            <span class="day">Friday</span>
                            <strong>27/12/2017</strong>
                            <p>At St. Thomas's Church, London, U.K.</p>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-4">
                        <div class="persons">
                            <figure><img src="images/about2.png" alt=""/></figure>
                            <div class="content">
                                <a href="#"><img src="images/about2.png" alt=""/></a>
                                <h4>( S/o Mrs fariza & Mr. jany )</h4>
                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit connec.</p>
                                <ul class="socials">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-skype"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>