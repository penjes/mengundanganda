<section class="family-area section bg-img jarallax af">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="section_heading">
                            <p>ARE GETTING MARRIED!</p>
                            <h2>LOVABLE FAMILY</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="family-tabbox">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
									<a href="#groom" aria-controls="groom" role="tab" data-toggle="tab">
										<img src="images/sm-1.jpg" alt=""/>
									</a>
								</li>
                                <li role="presentation">
									<a href="#bride" aria-controls="bride" role="tab" data-toggle="tab">
										<img src="images/sm-2.jpg" alt=""/>
									</a>
								
								</li>
                            </ul>
                            <!-- Nav tabs End-->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="groom">
                                    <div class="familyslider">
                                        <div class="item love col-md-3 col-sm-6">
                                            <figure><img src="images/family1.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Mr. Plera Kplex </h2>
                                                <p>(Joahn’s Father)</p>
                                            </div>
                                        </div>
                                        <div class="item love col-md-3 col-sm-6">
                                            <figure><img src="images/family1.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Jallifar Kplex</h2>
                                                <p>(Joahn’s Mother)</p>
                                            </div>
                                        </div>
                                        <div class="item love col-md-3 col-sm-6">
                                            <figure><img src="images/family1.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Mr. Plera kplex </h2>
                                                <p>(Joahn’s Father)</p>
                                            </div>
                                        </div>
                                        <div class="item br_rn col-md-3 col-sm-6">
                                            <figure><img src="images/family1.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Mr. Plera kplex </h2>
                                                <p>(Joahn’s Mother)</p>
                                            </div>
                                        </div>
                                        <div class="item col-md-3 col-sm-6">
                                            <figure><img src="images/family1.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Jonsina Kplix</h2>
                                                <p>(Joahn’s Brother)</p>
                                            </div>
                                        </div>
                                        <div class="item col-md-3 col-sm-6">
                                            <figure><img src="images/family1.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Palxia Kplix</h2>
                                                <p>(Joahn’s Sister)</p>
                                            </div>
                                        </div>
                                        <div class="item col-md-3 col-sm-6">
                                            <figure><img src="/images/family1.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Mr. Plera Kplex </h2>
                                                <p>(Joahn’s Brother)</p>
                                            </div>
                                        </div>
                                        <div class="item br_rn col-md-3 col-sm-6">
                                            <figure><img src="images/family1.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Mr. Plera Kplex </h2>
                                                <p>(Joahn’s Sister)</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="bride">
                                    <div class="familyslider">
                                        <div class="item love col-md-3 col-sm-6">
                                            <figure><img src="images/family2.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Mr. Plera Kplex </h2>
                                                <p>(Joahn’s Father)</p>
                                            </div>
                                        </div>
                                        <div class="item love col-md-3 col-sm-6">
                                            <figure><img src="images/family2.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Jallifar Kplex</h2>
                                                <p>(Joahn’s Mother)</p>
                                            </div>
                                        </div>
                                        <div class="item love col-md-3 col-sm-6">
                                            <figure><img src="images/family2.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Mr. Plera kplex </h2>
                                                <p>(Joahn’s Father)</p>
                                            </div>
                                        </div>
                                        <div class="item br_rn col-md-3 col-sm-6">
                                            <figure><img src="images/family2.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Mr. Plera kplex </h2>
                                                <p>(Joahn’s Mother)</p>
                                            </div>
                                        </div>
                                        <div class="item col-md-3 col-sm-6">
                                            <figure><img src="images/family2.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Jonsina Kplix</h2>
                                                <p>(Joahn’s Brother)</p>
                                            </div>
                                        </div>
                                        <div class="item col-md-3 col-sm-6">
                                            <figure><img src="images/family2.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Palxia Kplix</h2>
                                                <p>(Joahn’s Sister)</p>
                                            </div>
                                        </div>
                                        <div class="item col-md-3 col-sm-6">
                                            <figure><img src="images/family2.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Mr. Plera Kplex </h2>
                                                <p>(Joahn’s Brother)</p>
                                            </div>
                                        </div>
                                        <div class="item br_rn col-md-3 col-sm-6">
                                            <figure><img src="images/family2.jpg" alt=""/></figure>
                                            <div class="content">
                                                <h2>Mr. Plera Kplex </h2>
                                                <p>(Joahn’s Sister)</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>