<!doctype html>
<html class="no-js" lang="en">
    
<!-- Mirrored from www.webstrot.com/html/wedding/05_winter_wedding/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Oct 2018 09:05:59 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<?php $this->load->view('front/partial/head');?>
    <body>
    
		<div id="preloader">
			<div id="status"><img src="<?php echo base_url();?>images/preloader.gif" id="preloader_image" alt="loader">
			</div>
		</div>


        <!--Header area start here-->
        <?php $this->load->view('front/partial/navigation');?>
        <!--Header area end here-->
        <!--Slider area start here-->
        <?php $this->load->view('front/partial/slider');?>
        <!--Slider area End here-->
        <!--Counter area start here-->
        <section class="counter-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="content">
                            <p>Missing Days to</p>
                            <h2>Our Wedding</h2>
                            <span><img src="images/1.png" alt=""/></span>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="counter-list">
                            <ul>
                                <li><div class="count-con"><span class="days">00</span><p>Days</p></div></li>
                                <li><div class="count-con"><span class="hours">00</span><p>hours</p></div></li>
                                <li><div class="count-con"><span class="minutes">00</span><p>minutes</p></div></li>
                                <li><div class="count-con"><span class="seconds">00</span><p>seconds</p></div></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Counter area End here-->
        <!--persons area start here-->
        <?php $this->load->view('front/div_1');?>
        <!--persons area End here-->
        <!--Family area start here-->
        <?php $this->load->view('front/div_2');?>
        
        <!--Family area End here-->
        <!--Love story area start here-->
        <?php $this->load->view('front/div_3');?>
        <!--Love story area End here-->

        <!--Testimonial area start here-->
        <?php $this->load->view('front/div_3a');?>
        <!--Testimonial area End here--> 
        <!--Rspv area start here-->
        <?php $this->load->view('front/div_4.php');?>
        <!--Rspv area End here-->
        <!--Event area start here-->
        <?php $this->load->view('front/div_5');?>
        <!--Event area End here-->
        <!--Gallery wrapper start here-->
            <?php $this->load->view('front/div_6');?>
        <!--Gallery wrapper End here-->

        <!--Contact area start here-->
        <!--Footer wrapper start here-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="foo-logo">
                            <a href="index.html">
                                <img src="images/logo2.png" alt=""/></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <div class="copyright">
                            <p>@ Copyright <?php echo date('Y');?> All Rights Reserved. By mengundanganda.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--Footer wrapper end here-->


		<!-- All JavaScript Here -->

		<!-- jQuery latest version -->
        <script src="<?php echo base_url();?>assets/jquery-3.2.1.min.js"></script>
		<!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url();?>assets/bootstrap.min.js"></script>
        <!-- Owl.carousel JavaScript -->
        <script src="<?php echo base_url();?>assets/owl.carousel.min.js"></script>
        <!-- Bxslider JavaScript -->
        <script src="<?php echo base_url();?>assets/jquery.bxslider.min.js"></script>
        <!-- Magnific Popup JavaScript -->
        <script src="<?php echo base_url();?>assets/jquery.magnific-popup.min.js"></script>
		<!-- meanmenu JavaScript -->
        <script src="<?php echo base_url();?>assets/jquery.meanmenu.js"></script>
        <!-- jarallax JavaScript -->
        <script src="<?php echo base_url();?>assets/jarallax.min.js"></script>
		<!-- jQuery-ui JavaScript -->
        <script src="<?php echo base_url();?>assets/jquery-ui.min.js"></script>
        <!-- masonry JavaScript -->
        <script src="<?php echo base_url();?>assets/masonry.pkgd.min.js"></script>
        <!-- downCount JavaScript -->
        <script src="<?php echo base_url();?>assets/jquery.downCount.js"></script>
		<!-- wow JavaScript -->
        <script src="<?php echo base_url();?>assets/wow.min.js"></script>
		<!-- Plugins JavaScript -->
        <script src="<?php echo base_url();?>assets/plugins.js"></script>
		<!-- Init JavaScript -->
        <script src="<?php echo base_url();?>assets/main.js"></script>

    </body>

<!-- Mirrored from www.webstrot.com/html/wedding/05_winter_wedding/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Oct 2018 09:05:59 GMT -->
</html>
