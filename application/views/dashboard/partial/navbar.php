 <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Dashboard</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url();?>assets/dashboard/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $this->session->userdata('users');?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li>
                    <a href="<?php echo base_url();?>admin">
                      <i class="fa fa-home"></i> Home 
                    </a>
                    
                  </li>
                  <li>
                    <a>
                      <i class="fa fa-edit"></i> Konten <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>admin/events">Jadwal Acara</a></li>
                      <li><a href="<?php echo base_url();?>admin/our_story">Cerita Kita</a></li>
                      <li><a href="<?php echo base_url();?>admin/galleri">Galleri Foto</a></li>
                      <li><a href="<?php echo base_url();?>admin/reviews">Buku Tamu</a></li>
                      <li><a href="<?php echo base_url();?>admin/configuration">Pengaturan Konten</a></li>
                      <li><a href="<?php echo base_url();?>admin/is_page">Konten</a></li>
                      <?php if(empty($_SESSION['kode_mirror_internal'])){?>
                      <li>
                        <a href="<?php echo base_url();?>admin/mirror_login">Mirror Login</a>
                      <?php } else{ ?>
                        <a href="<?php echo base_url();?>admin/mirror_login/unset_mirror_login">Cancel Mirror Login</a>
                      <?php } ?>
                      </li>
                      <li>
                          <a href="<?php echo base_url();?>admin/rekening_q">Bank</a></li>
                       </li>
                    </ul>
                  </li>
                  
                   
                </ul>
              </div>
             <!--  -->

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo  base_url();?>admin">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>