
              <div class="x_panel">
                 <?php echo $this->session->flashdata('msg');?>
                <div class="x_title">
                  <h2>Konten &nbsp;&nbsp;&nbsp;</h2>
                 
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li> 
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <ul class="list-unstyled timeline">
                    <?php 
                    $no=1;
                    foreach ($is_page as $key) { ?>
                    <li>
                      <div class="block">
                        <div class="tags">
                          <a href="" class="tag">
                            <span><?php echo $key->page_name;?></span>
                          </a>
                        </div>
                        <div class="block_content">
                          <h2 class="title">
                                          <a><?php echo $key->head_title;?></a>
                          </h2>
                          <div class="byline">
                            <span><?php echo $key->title;?>
                          </div>
                          <p class="excerpt"><?php echo $key->content;?>
                            &nbsp;&nbsp;
                            
                            <a href="<?php echo base_url();?>admin/is_page/status/<?php echo encode_id($key->id);?>" title='status' onclick="return confirm('Anda yakin ingin menghilangkan konten ini ?');"><i class="fa fa-eye-slash"></i></a> |
                            <a href="<?php echo base_url();?>admin/is_page/edit/<?php echo encode_id($key->id);?>/<?php echo $no;?>" title='Rubah Konten'><i class="fa fa-edit"></i></a>
                          </p>
                        </div>
                      </div>
                    </li>
                    <?php $no++; } ?> 
                  </ul>

                </div>
                <a href="<?php echo base_url();?>admin/is_page/add" class="btn btn-primary" >Tambah Konten</a>
              </div>
          
<div id="add_form">
</div>
<script type="text/javascript">
function add(category){
  var base_url ='<?php echo base_url();?>';
  $.ajax({
        url : base_url+"admin/events/add",
        type : "POST",
        cache : false,
        beforeSend: function() {
           $('#add_form').html("<div class='box'><div style='margin:10px auto;text-align:center;'><img src='"+base_url+"assets/images/loader.gif'><br><b>Please Wait....</b></div></div>");
        },
        success : function(data){
            $('#add_form').html(data);
        },
        error: function (request,status,error){
            alert(request.responseText);
        }
    });
}

</script>