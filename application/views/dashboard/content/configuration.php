 <div class="x_panel">
 <div class="x_title">
                    <h2>Biodata <small>Anda</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li> 
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
<?php if(empty($biodata)){ ?>
<div class="x_content">

<h5 class="center">Masa Berlaku sudah habis, silahkan melakukan order paket baru untuk mengaktifkan halaman ini</h5>
</div>
<?php }else{ ?>
<div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                       <h4><b>Acara Resepsi</b></h4><hr>

                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-heart user-profile-icon"></i> 
                          <?php echo $biodata->acara .'  ( '.substr($biodata->start_at,0,5).' - '.substr($biodata->end_at,0,5).' )';?>
                        </li>

                        <li><i class="fa fa-map-marker user-profile-icon"></i> 
                          <?php echo $biodata->alamat.', '.$biodata->kecamatan.'  '.$biodata->kota;?>
                        </li>

                        <li>
                          <i class="fa fa-envelope user-profile-icon"></i> <?php echo $biodata->email;?>
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-phone-square user-profile-icon"></i>&nbsp;
                          <?php echo $biodata->telp;?>
                        </li>
                        <li class="m-top-xs">
                          <img src="<?php echo base_url();?>assets/images/whatsapp.png" style="width:14px;"> &nbsp;
                          <?php echo $biodata->whatsapp.' (Whatsapp)';?>
                        </li>
                        <li class="m-top-xs">
                            <?php if(!empty($biodata->status)){?>
                                <button type="button" class="btn btn-success btn-xs">Aktif</button>
                            <?php }else{ ?>
                                <a href="#" class="btn btn-danger btn-xs">Non Aktif </a>
                            <?php } ?>
                        </li>
                      </ul>
                   </div>





                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Orang Tua Mempelai Pria</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Orang Tua Mempelai Wanita</a> 
                          </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                            <!-- start recent activity -->
                            <ul class="messages">
                              <li>
                                <div class="message_wrapper">
                                  <h5 class="heading">Putra dari Keluarga</h5>
                                  <blockquote class="message">Bapak. <?php echo $biodata->ayah_pria;?></blockquote>
                                  <br> 
                                </div>
                              </li>
                              <li>
                                <div class="message_wrapper">
                                  <h5 class="heading">Putra dari Keluarga</h5>
                                  <blockquote class="message">Ibu. <?php echo $biodata->ibu_pria;?></blockquote>
                                  <br> 
                                </div>
                              </li>
                              

                            </ul>
                            <!-- end recent activity -->

                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                            <ul class="messages">
                              <li>
                                <div class="message_wrapper">
                                  <h5 class="heading">Putri dari Keluarga</h5>
                                  <blockquote class="message">Bapak. <?php echo $biodata->ayah_wanita;?></blockquote>
                                  <br> 
                                </div>
                              </li>
                              <li>
                                <div class="message_wrapper">
                                  <h5 class="heading">Putri dari Keluarga</h5>
                                  <blockquote class="message">Ibu. <?php echo $biodata->ibu_wanita;?></blockquote>
                                  <br> 
                                </div>
                              </li>
                              

                            </ul>
                          </div> 
                        </div>
                      </div>

                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 center">
                      <a href="#" class="btn btn-warning"><i class="fa fa-info m-right-xs"></i>&nbsp;Bantuan</a>

                      <a href="<?php echo base_url();?>admin/configuration/<?php echo $action;?>/<?php echo encode_id($biodata->id);?>" class="btn btn-primary">
                        <i class="fa fa-edit m-right-xs"></i>&nbsp;Edit Profil</a>                      
                      <a href="https://wa.me/?text=<?php echo base_url();?>" class="btn btn-success">
                        <img src="<?php echo base_url();?>assets/images/whatsapp.png" style="width:14px;">&nbsp;Share undangan</a>
                 
                    </div>


                    <div class="clearfix"></div>
                    <hr>
                    <h2 class="center">Kedua Mempelai</h2>
                    <div class="col-md-12 col-sm-12 col-xs-12" style="border: solid 1px #d8d0d0;padding:15px;">
                      
                      <!-- -->
                      <div class="col-md-6 col-sm-6 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>Mempelai Pria</i></h4>
                            <div class="left col-xs-7">
                              <h2><?php echo $biodata->mempelai_pria;?></h2>
                               <?php echo $biodata->content_pria;?>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="<?php echo image_src($biodata->photo_pria,'manten');?>" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                               
                            </div>
                            <div class="col-xs-12 col-sm-6 emphasis">
                              <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                                </i> <i class="fa fa-comments-o"></i> </button>
                              <a href="<?php echo base_url();?>admin/configuration/edit_photo/<?php echo encode_id($biodata->id);?>/<?php echo encode_id('pria');?>" class="btn btn-primary btn-xs">
                                <i class="fa fa-file-image-o"> </i> Upload Foto
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>


                      <div class="col-md-6 col-sm-6 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>Mempelai Wanita</i></h4>
                            <div class="left col-xs-7">
                              <h2><?php echo $biodata->mempelai_wanita;?></h2>
                               <?php echo $biodata->content_wanita;?>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="<?php echo image_src($biodata->photo_wanita,'manten');?>" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                               
                            </div>
                            <div class="col-xs-12 col-sm-6 emphasis">
                                  <button type="button" class="btn btn-success btn-xs"> 
                                    <i class="fa fa-user">
                                    </i> 
                                    <i class="fa fa-comments-o"></i> </button>
                                  <a href="<?php echo base_url();?>admin/configuration/edit_photo/<?php echo encode_id($biodata->id);?>/<?php echo encode_id('wanita');?>" class="btn btn-primary btn-xs">
                                    <i class="fa fa-file-image-o"> </i> Upload Foto
                                  </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      
                    </div>
                <?php if($biodata->is_template =='TEMP01'){ ?>
                            <div class="clearfix"></div><br>
                            <div class="center">
                                <h4><i class="fa fa-image"></i>&nbsp;Gambar Utama</h4>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12" style="border: solid 1px #d8d0d0; margin:10px auto;padding:10px;">
                                <img class="img-responsive avatar-view" src="<?php echo image_src($biodata->bg,'bg');?>" alt="Backgroud" title="Backgroud">
                            </div>
                            <div class="row">
                                 <div class="col-md-12 col-sm-12 col-xs-12 center" style="padding:10px 50px !important;">
                                                <a href="<?php echo base_url();?>admin/slider/edit/<?php echo encode_id($biodata->id);?>/<?php echo encode_id('wanita');?>" class="btn btn-success"><i class="fa fa-file-image-o m-right-xs"></i>&nbsp;&nbsp;Upload Foto</a>
                                 </div> 
                             </div>

                   <?php }else{ ?>
                   <div class="clearfix"></div><br>
                   <div class="col-md-12 col-sm-12 col-xs-12">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>No.</th>
                            <th>Background</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $no=1;
                          if(!empty($slider))
                                { 
                                  foreach ($slider as $key) {
                                    ?>
                                      <tr>
                                        <td><?php echo $no;?></td>
                                        <td>
                                          <div  class="col-sm-12" >
                                            <img class="img-responsive" width="500" src="<?php echo image_src($key->image,'bg');?>" alt="Backgroud" title="Backgroud">
                                          </div>
                                        </td>  
                                        <td>
                                          <a href="<?php echo base_url();?>admin/slider/rubah/<?php echo encode_id($key->id);?>" title="Rubah Foto" class="btn btn-success">
                                            <i class="fa fa-file-image-o m-right-xs"></i>
                                          </a>
                                          <a href="<?php echo base_url();?>admin/slider/hapus/<?php echo encode_id($key->id);?>" title="Hapus Foto" class="btn btn-danger" onclick="return confirm('Anda yakin ingin menghapus data ini ?');">
                                            <i class="fa fa-trash m-right-xs"></i>
                                          </a>
                                        </td>
                                      </tr>
                                      <?php $no++;
                                    }
                                }else{ ?>
                                  <tr>
                                    <td colspan="3">
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert alert-danger">Data Kosong</div></td>
                                      </div>
                                  </tr>
                                <?php } ?>
                        </tbody>
                      </table>
                   </div>
                   <hr>
                   <div class="col-md-12 col-sm-12 col-xs-12 center">
                    <a href="<?php echo base_url();?>admin/slider/addslider/<?php echo encode_id($biodata->id);?>" class="btn btn-success">
                      <i class="fa fa-file-image-o m-right-xs"></i>&nbsp;&nbsp;Upload Foto</a>
                   </div>
                   <?php } ?>
                  </div>
                  </div>

<?php }?>