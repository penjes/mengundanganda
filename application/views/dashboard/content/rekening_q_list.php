<div class="x_panel">
                  <div class="x_title">
                    <h2>Rekening Bank  </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                     <div class="row" style="margin: 20px 10px">
                        <div class="col-md-4">
                            <?php echo anchor(site_url('admin/rekening_q/create'),'<i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Kode Rekening', 'class="btn btn-primary"'); ?>
                        </div>
                        <div class="col-md-4 text-center">
                            <div style="margin-top: 8px" id="message">
                                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                            </div>
                        </div>
                        <div class="col-md-1 text-right">
                        </div>
                        <div class="col-md-3 text-right">
                            <form action="<?php echo site_url('rekening_q/index'); ?>" class="form-inline" method="get">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                                    <span class="input-group-btn">
                                        <?php 
                                            if ($q <> '')
                                            {
                                                ?>
                                                <a href="<?php echo site_url('rekening_q'); ?>" class="btn btn-default">Reset</a>
                                                <?php
                                            }
                                        ?>
                                      <button class="btn btn-primary" type="submit">Search</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>


                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div class="clearfix"></div>
                            <div class="alert alert-warning">
                                <strong>caution :</strong> Size upload image Recomended width(166) & height=(233)
                            </div>
                            <table class="table table-bordered" style="margin-bottom: 10px">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Bank</th>
                                    <th>Atas Nama</th>
                                    <th>Photo</th>
                                    <th>Kode Rekening</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr><?php
                                foreach ($rekening_q_data as $rekening_q)
                                {
                                    ?>
                                    <tr>
                                          <td width="80px"><?php echo ++$start ?></td>
                                          <td><?php echo $rekening_q->nama_bank ?></td>
                                                <td><?php echo $rekening_q->atas_nama ?></td>
                                          <td width="10%">
                                                    <!-- <div class="img-cntr"> -->
                                                         <img src="<?php echo image_src($rekening_q->photo,'rekening') ?>" class="img-responsive">
                                                    <!-- </div> -->
                                                    <div class="text_center">
                                                        <a href="<?php echo base_url();?>admin/rekening_q/delete_image/<?php echo encode_id($rekening_q->id);?>" onclick="return confirm('Anda yakin ingin menghapus gambar ini ?');"  >
                                                            <i class="fa fa-trash"></i>
                                                        </a>&nbsp; | &nbsp; 
                                                        <a href="#" data-toggle="modal" data-target="#caption_<?php echo  $rekening_q->id;?>">
                                                            <i class='fa fa-edit'></i>
                                                        </a>
                                                    </div>
                                            </td>
                                                          <div class="modal fade" id="caption_<?php echo  $rekening_q->id;?>" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel">
                                                              <div class="modal-dialog" role="document">                                                                                          
                                                                  <?php echo form_open_multipart('admin/rekening_q/ubah_photo');?>      
                                                                  <div class="modal-content">
                                                                          <div class="modal-header">
                                                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                              <h4 class="modal-title" id="uploadModalLabel">Edit Photo</h4>
                                                                          </div>
                                                                          <div class="modal-body">
                                                                              <div class="row">
                                                                                  <div class="col-md-12 col-sm-12">
                                                                                      <div class="col-md-12 col-sm-12 padd-10">
                                                                                          <div class="col-md-6 col-sm-12 padd-10 text_center_small">
                                                                                              <img class="img-responsive"  src="<?php echo image_src($rekening_q->photo,'rekening');?>"  width="200" height="200" id="gambar<?php echo $rekening_q->id;?>"> 

                                                                                          </div>
                                                                                          <div class="col-md-6 col-sm-12 padd-10 text_center_small">
                                                                                              <div class="upload-btn-wrapper ">
                                                                                                <button class="btn btn-danger">Browse</button>
                                                                                                <input type="file" name="photos_review" id="preview_gambar<?php echo  $rekening_q->id;?>" onchange="previews(this,<?php echo  $rekening_q->id;?>)") />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-6 col-sm-12">

                                                                                      <input type="hidden" name="id" value="<?php echo  $rekening_q->id;?>">
                                                                                      <input type="hidden" name="photo_old" value="<?php echo $rekening_q->photo;?>"> 
                                                                                  </div>
                                                                              </div>
                                                                          </div>
                                                                      </div>
                                                                      <div class="modal-footer">
                                                                          <button type="submit" class="btn btn-primary">Save</button>
                                                                          <button type="button" class="btn default radius-btn" data-dismiss="modal">Close</button>
                                                                      </div>
                                                                  </div>
                                                                <?php echo form_close();?>
                                                                <!-- </form> -->
                                                            </div>
                                                          </div>
                                       <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                                        <td><?php echo $rekening_q->kode_rekening ?></td>
                                        <td><?php if(empty($rekening_q->status)){ echo 'NonAktif';}else{ echo 'Aktif';} ?></td>
                                        <td style="text-align:center" width="200px">
                                          <?php 
                                         echo anchor(site_url('admin/rekening_q/update/'.encode_id($rekening_q->id)),'Update'); 
                                          echo ' | '; 
                                          echo anchor(site_url('admin/rekening_q/delete/'.encode_id($rekening_q->id)),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                                          ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
                                    <?php echo anchor(site_url('admin/rekening_q/excel'), 'Excel', 'class="btn btn-primary"'); ?>
                                </div>
                                <div class="col-md-6 text-right">
                                    <?php echo $pagination ?>
                                </div>
                            </div>

                  </div>
</div>

<script type="text/javascript">
 function previews(gambar,idgambar){
    // gambar.files;
    var reader = new FileReader();

    reader.onload= function(e){
        $('#gambar'+idgambar).attr('src',e.target.result);
    }
    reader.readAsDataURL(gambar.files[0]);
 }
</script> 