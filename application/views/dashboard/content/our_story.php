<div class="x_panel">
                  <div class="x_title">
                    <h2>Perjalanan Cinta Kita</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <?php foreach ($our_story as $key) { ?>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <div class="product-image">
                                <img src="<?php echo image_src($key->image,'story');?>" alt="...">
                              </div>
                             
                            </div>

                            <div class="col-md-9 col-sm-9 col-xs-12" style="border:0px solid #e5e5e5;">
                              <h3 class="prod_title"><?php echo $key->title;?></h3>

                              <p><?php echo $key->content;?></p>
                              <?php echo 'no. '.$key->urutan;?>
                              <br>
                              <div>
                                <h2><?php echo $key->history_date;?></h2>
                                 <?php if($key->tampilkan_date_history){?>
                                 <a href="#" class="btn btn-success btn-xs">Tanggal ditampilkan </a> 
                                        
                                 <?php }else{ ?>
                                 <a href="#" class="btn btn-danger btn-xs">Tanggal tidak ditampilkan </a> 
                                 <?php } ?>
                              </div>
                            </div>
                        </div>
                        <div class="clearfix"></div><br>
                     <?php } ?>
                     <div class="clearfix"></div>
                     <br><br>
                      <div class="col-md-12 col-sm-12 col-xs-12" style="border:0px solid #e5e5e5;">    
                          <a href="<?php echo base_url();?>admin/our_story/add" class="btn btn-primary" >Tambah</a>
                      </div>
                  </div>
                </div>