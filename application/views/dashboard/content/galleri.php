<div class="x_panel">
    <?php echo $this->session->flashdata('msg');?>
               
                  <div class="x_title">
                    <h2>Media Gallery <small> gallery design </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row">

                      <p>Gallery Foto</p>
                      <?php foreach ($photo as $key){ ?>
                      <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="<?php echo image_src($key->image,'galleri');?>" alt="<?php echo $key->title;?>" />
                            <div class="mask">
                              <p><?php echo $key->title;?></p>
                              <div class="tools tools-bottom">
                                <a href="<?php echo base_url();?>admin/galleri/edit/<?php echo encode_id($key->id);?>"><i class="fa fa-pencil"></i></a> 
                                 | 
                                <a href="<?php echo base_url();?>admin/galleri/delete/<?php echo encode_id($key->id);?>" title='Hapus' onclick="return confirm('Anda yakin ingin menghapus data ini ?');">
                                  <i class="fa fa-trash"></i></a>
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p><?php echo $key->subtitle;?></p>
                          </div>
                        </div>
                      </div>
                      <?php } ?>

                    </div>
                    <a href="<?php echo base_url();?>admin/galleri/add" class="btn btn-primary" >Upload Foto</a>
                  </div>
                </div>