<div class="x_panel">
                  <div class="x_title">
                    <h2>Acara ke - <?php echo $acara;?> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form action="<?php echo base_url();?>admin/events/<?php echo $action;?>" method="post" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Judul<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="title" name="title" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $title;?>'>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Isi Kontent</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="description" class="form-control col-md-7 col-xs-12" type="text" name="description" value='<?php echo $description;?>'>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name='event_date' class="form-control has-feedback-left" id="single_cal1" value="<?php echo $event_date;?>" aria-describedby="inputSuccess2Status">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Mulai</label>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <select class="select2_single form-control" tabindex="-1" name ="start_1">
                            
                            <?php for($i=0;$i<=23;$i++){?>
                            <option value="<?php 
                                if(strlen($i)==1){
                                    echo '0'.$i; }
                                else{
                                    echo $i;
                                  }
                                  ?>" 
                                  <?php if(strlen($i)==1){ if($start_1 =='0'.$i){ echo 'selected';}}else{ if($start_1 ==$i){ echo 'selected';} }?>>
                                  <?php
                                      if(strlen($i)==1){
                                          echo '0'.$i; }
                                      else{
                                          echo $i;
                                        } 
                                    ?>
                              </option>
                            <?php } ?>
                          </select>
                        </div> : 
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <select class="select2_single form-control" tabindex="-1" name ="start_2">
                            
                            <?php for($i=0;$i<=59;$i++){?>
                            <option value="<?php 
                                if(strlen($i)==1){
                                    echo '0'.$i; }
                                else{
                                    echo $i;
                                  }
                                  ?>" <?php if(strlen($i)==1){ if($start_2 =='0'.$i){ echo 'selected';}}else{ if($start_2 ==$i){ echo 'selected';} } ?>>
                                  <?php
                                      if(strlen($i)==1){
                                          echo '0'.$i; }
                                      else{
                                          echo $i;
                                        } 
                                    ?>
                              </option>
                            <?php } ?>
                          </select>
                        </div>
                        
                      </div>
                      <div class="form-group">
                        <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Selesai</label>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <select class="select2_single form-control" tabindex="-1" name ="end_1">
                            
                            <?php for($i=0;$i<=23;$i++){?>
                            <option value="<?php 
                                if(strlen($i)==1){
                                    echo '0'.$i; }
                                else{
                                    echo $i;
                                  }
                                  ?>" <?php if(strlen($i)==1){ if($end_1 =='0'.$i){ echo 'selected';}}else{ if($end_1 ==$i){ echo 'selected';} } ?>>
                                  <?php
                                      if(strlen($i)==1){
                                          echo '0'.$i; }
                                      else{
                                          echo $i;
                                        } 
                                    ?>
                              </option>
                            <?php } ?>
                          </select>
                        </div> : 
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <select class="select2_single form-control" tabindex="-1" name ="end_2">
                            
                            <?php for($i=0;$i<=59;$i++){?>
                            <option value="<?php 
                                if(strlen($i)==1){
                                    echo '0'.$i; }
                                else{
                                    echo $i;
                                  }
                                  ?>" <?php if(strlen($i)==1){ if($end_2 =='0'.$i){ echo 'selected';}}else{ if($end_2 ==$i){ echo 'selected';} } ?>>
                                  <?php
                                      if(strlen($i)==1){
                                          echo '0'.$i; }
                                      else{
                                          echo $i;
                                        } 
                                    ?>
                              </option>
                            <?php } ?>
                          </select>
                        </div>
                        
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <input type="hidden" name='ids' value="<?php echo $ids;?>">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a class="btn btn-primary" href="<?php echo base_url();?>admin/events">Cancel</a>
						              <button class="btn btn-success" type="submit">Submit</button> 
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
