<div class="x_panel">
                  <div class="x_title">
                    <h2>Form validation <small>sub title</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                   <form action="<?php echo base_url();?>admin/configuration/<?php echo $action;?>" method="post" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                      
                      <span class="section">Profil Info</span>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" id="email" name="email" value="<?php echo $email;?>" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Telp <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="telp" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="telp" required="required" type="number" value="<?php echo $telp;?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Whatsapp <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="whatsapp" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="whatsapp" required="required" type="number" value="<?php echo $whatsapp;?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Mempelai Pria<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="mempelai_pria" name="mempelai_pria" value="<?php echo $mempelai_pria;?>" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Mempelai Wanita <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="mempelai_wanita" name="mempelai_wanita" value="<?php echo $mempelai_wanita;?>" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Ayah dari Mempelai Pria <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="ayah_pria" name="ayah_pria" value="<?php echo $ayah_pria;?>" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Ibu dari Mempelai Wanita <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="ibu_pria" name="ibu_pria" value="<?php echo $ibu_pria;?>" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                        

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Ayah dari Mempelai Wanita <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="ayah_wanita" name="ayah_wanita" value="<?php echo $ayah_wanita;?>" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Ibu dari Mempelai Wanita <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="ibu_wanita" name="ibu_wanita" value="<?php echo $ibu_wanita;?>" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label for="password" class="control-label col-md-3">Alamat</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="alamat" type="text" name="alamat" value="<?php echo $alamat;?>" data-validate-length="6,8" class="form-control col-md-7 col-xs-12" required="required">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name='acara_resepsi' class="form-control has-feedback-left" id="single_cal1" value="<?php echo $acara_resepsi;?>" aria-describedby="inputSuccess2Status">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Mulai</label>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <select class="select2_single form-control" tabindex="-1" name ="start_1">
                            
                            <?php for($i=0;$i<=23;$i++){?>
                            <option value="<?php 
                                if(strlen($i)==1){
                                    echo '0'.$i; }
                                else{
                                    echo $i;
                                  }
                                  ?>" 
                                  <?php if(strlen($i)==1){ if($start_1 =='0'.$i){ echo 'selected';}}else{ if($start_1 ==$i){ echo 'selected';} }?>>
                                  <?php
                                      if(strlen($i)==1){
                                          echo '0'.$i; }
                                      else{
                                          echo $i;
                                        } 
                                    ?>
                              </option>
                            <?php } ?>
                          </select>
                        </div> : 
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <select class="select2_single form-control" tabindex="-1" name ="start_2">
                            
                            <?php for($i=0;$i<=59;$i++){?>
                            <option value="<?php 
                                if(strlen($i)==1){
                                    echo '0'.$i; }
                                else{
                                    echo $i;
                                  }
                                  ?>" <?php if(strlen($i)==1){ if($start_2 =='0'.$i){ echo 'selected';}}else{ if($start_2 ==$i){ echo 'selected';} } ?>>
                                  <?php
                                      if(strlen($i)==1){
                                          echo '0'.$i; }
                                      else{
                                          echo $i;
                                        } 
                                    ?>
                              </option>
                            <?php } ?>
                          </select>
                        </div>
                        
                      </div>
                      <div class="form-group">
                        <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Selesai</label>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <select class="select2_single form-control" tabindex="-1" name ="end_1">
                            
                            <?php for($i=0;$i<=23;$i++){?>
                            <option value="<?php 
                                if(strlen($i)==1){
                                    echo '0'.$i; }
                                else{
                                    echo $i;
                                  }
                                  ?>" <?php if(strlen($i)==1){ if($end_1 =='0'.$i){ echo 'selected';}}else{ if($end_1 ==$i){ echo 'selected';} } ?>>
                                  <?php
                                      if(strlen($i)==1){
                                          echo '0'.$i; }
                                      else{
                                          echo $i;
                                        } 
                                    ?>
                              </option>
                            <?php } ?>
                          </select>
                        </div> : 
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <select class="select2_single form-control" tabindex="-1" name ="end_2">
                            
                            <?php for($i=0;$i<=59;$i++){?>
                            <option value="<?php 
                                if(strlen($i)==1){
                                    echo '0'.$i; }
                                else{
                                    echo $i;
                                  }
                                  ?>" <?php if(strlen($i)==1){ if($end_2 =='0'.$i){ echo 'selected';}}else{ if($end_2 ==$i){ echo 'selected';} } ?>>
                                  <?php
                                      if(strlen($i)==1){
                                          echo '0'.$i; }
                                      else{
                                          echo $i;
                                        } 
                                    ?>
                              </option>
                            <?php } ?>
                          </select>
                        </div>
                        
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <input type="hidden" name='ids' value="<?php echo $ids;?>">
                          <a class="btn btn-primary" href="<?php echo base_url();?>admin/configuration">Cancel</a>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>