 <div class="x_panel">
                  <div class="x_title">
                    <h2>Default Example <small>Users</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table class="table table-striped table-bordered">
                      <thead>
                        <tr style="color:#777575;">
                          <th width="5%">No.</th>
                          <th>Nama</th>
                          <th width="55%">Kartu Ucapan</th>
                          <th>Waktu Kirim</th>
                          <th></th>
                        </tr>
                      </thead>


                      <tbody>
                        <?php $no=$no++; 
                        if(!empty($data)) { 
                                foreach ($data as $key){ ?>
                                  <tr>
                                    <td><?php echo $no;?></td>
                                    <td><?php echo $key->nama;?></td>
                                    <td><?php echo $key->reviews;?></td>
                                    <td></td>
                                  </tr> 
                      <?php       $no++;} 
                              }else{
                                echo "<tr><td colspan='5'><div class='alert alert-danger'>Data Kosong</div></tr>";
                              }
                        ?>
                      </tbody>
                    </table>
                    <?php if(!empty($pagination)){?>
                    <div class="col-md-6 text-right">
                        <?php echo $pagination ?>
                    </div>
                    <?php } ?>
                  </div>
                </div>