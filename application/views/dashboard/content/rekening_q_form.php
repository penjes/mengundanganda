<div class="x_panel">
                  <div class="x_title">
                    <h2>Rekening Bank </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="row">
                          <form action="<?php echo $action; ?>" method="post" id="form-add-rekening">
                          <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                  <label for="varchar">Nama Bank <?php echo form_error('nama_bank') ?></label>
                                  <input type="text" class="form-control" name="nama_bank" id="nama_bank" placeholder="Nama Bank" value="<?php echo $nama_bank; ?>" />
                              </div>
                              <div class="form-group">
                                  <label for="varchar">Atas Nama <?php echo form_error('atas_nama') ?></label>
                                  <input type="text" class="form-control" name="atas_nama" id="atas_nama" placeholder="Atas Nama" value="<?php echo $atas_nama; ?>" />
                              </div>
                              <div class="form-group">
                                                         <label>Photo</label>
                                                          <div id="upload-review-photo">
                                                              <input id="upload-review" type="file" name="file" />
                                                          </div>
                                                          <br>
                              </div>
                          </div>
                          <div class="col-md-6 col-sm-6">
                                  <div class="form-group">
                                      <label for="varchar">Kode Rekening <?php echo form_error('kode_rekening') ?></label>
                                      <input type="text" class="form-control" name="kode_rekening" id="kode_rekening" placeholder="Kode Rekening" value="<?php echo $kode_rekening; ?>" />
                                  </div>
                                   <div class="form-group">
                                      <label for="int">Status <?php echo form_error('status') ?></label>
                                       <select name="status" class="form-control">
                                          <option value="1" <?php if($status=='1'){ echo'selected';}?>>Aktif</option>
                                          <option value="0" <?php if($status=='0'){ echo'selected';}?>>Non Aktif</option>
                                      </select>
                                  </div>
                                  <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                                  <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                                  <a href="<?php echo site_url('admin/rekening_q') ?>" class="btn btn-default">Cancel</a>
                              </form>
                          </div>




 
                  </div>
                </div>





<script>
$('#form-add-rekening').submit(function(ev) {
    ev.preventDefault();
var name = $('#atas_nama').val();
var nama_bank = $('#nama_bank').val();
var kode_rekening = $('#kode_rekening').val();

if (nama_bank=='') {
    alert('Nama Bank harus di isi');
    return false;
};
if (name=='') {
    alert('Nama Pemilik rekening harus di isi');
    return false;
};
if ((kode_rekening=='')||(kode_rekening < 1)) {

    alert('kode rekening tidak boleh kosong');
    return false;
};

this.submit(); // If all the validations succeeded
});

 $(document).ready(function() {
    $("#btn-upload-review").click(function() {
          $("#upload-review-photo").slideToggle("slow");
        });
        $('#uploadModal').on('hidden.bs.modal', function (e) {
          $( "#load-photo-review" ).load("<?php echo base_url();?>.'admin/Rekening_q/ajax_get_review_photos?rvid='.$my_review_id)?>");
        })
        // enable fileuploader plugin
        $('#upload-review').fileuploader({
            limit: 1,
            extensions: ['PNG','JPG','JPEG','jpg', 'jpeg', 'png', 'gif'],
            changeInput: '<div class="fileuploader-input">' +
                              '<div class="fileuploader-input-inner">' +
                                  '<!--<img src="<?=base_url('_assets/global/img/fileuploader-dragdrop-icon.png')?>">-->' +
                                  '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                                  '<p>or</p>' +
                                  '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                              '</div>' +
                          '</div>',
            theme: 'dragdrop',
            upload: {
                url: '<?php echo base_url();?>admin/Rekening_q/ajax_upload_photo',
                data: null,
                type: 'POST',
                enctype: 'multipart/form-data',
                start: true,
                synchron: true,
                beforeSend: null,
                onSuccess: function(result, item) {
                    var data = JSON.parse(result);
                    console.log(data);
                    // if success
                    if (data.isSuccess && data.files[0]) {
                        item.name = data.files[0].name;
                        item.html.find('.columns').append('<input type="hidden" name="photo[]" value="'+data.files[0].name+'"/>');
                       // item.html.find('.columns').append('<p>Caption : <input type="text" name="caption[]" value=""/></p>');
                    }
                    
                    // if warnings
                    if (data.hasWarnings) {
                        for (var warning in data.warnings) {
                            alert(data.warnings);
                        }
                        
                        item.html.removeClass('upload-successful').addClass('upload-failed');
                        // go out from success function by calling onError function
                        // in this case we have a animation there
                        // you can also response in PHP with 404
                        return this.onError ? this.onError(item) : null;
                    }
                    
                    item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                    setTimeout(function() {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);
                },
                onError: function(item) {
                    var progressBar = item.html.find('.progress-bar2');
                    
                    if(progressBar.length > 0) {
                        progressBar.find('span').html(0 + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
                        item.html.find('.progress-bar2').fadeOut(400);
                    }
                    
                    item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                        '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                    ) : null;
                },
                onProgress: function(data, item) {
                    var progressBar = item.html.find('.progress-bar2');
                    
                    if(progressBar.length > 0) {
                        progressBar.show();
                        progressBar.find('span').html(data.percentage + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                    }
                },
                onComplete: null,
            },
            onRemove: function(item) {
                $.post('<?php echo base_url();?>admin/Rekening_q/ajax_remove_photo', {
                    file: item.name
                });
            },
            captions: {
                feedback: 'Drag and drop files here',
                feedback2: 'Drag and drop files here',
                drop: 'Drag and drop files here'
            }
        });
        
               
    });

</script>