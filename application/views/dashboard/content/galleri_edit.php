<div class="x_panel">
                  <div class="x_title">
                    <h2>Acara ke - <?php echo $acara;?> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form action="<?php echo base_url();?>admin/galleri/<?php echo $action;?>" method="post" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Judul<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="title" name="title" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $title;?>'>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="subtitle" class="control-label col-md-3 col-sm-3 col-xs-12">Sub Judul</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="subtitle" class="form-control col-md-7 col-xs-12" type="text" name="subtitle" value='<?php echo $subtitle;?>'>
                        </div>
                      </div> 
                       <?php if(!empty($photo_old)){ ?>
                                  <div class="form-group"> 
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Rubah Foto</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="border: solid 1px #e4e2e2;">
                                        <img src="<?php echo image_src($photo_old,'galleri');?>" class="img-responsive">
                                    </div>
                                  </div>
                      <?php } ?>
                      <div class="form-group">
                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto</label>
                                        <div id="upload-review-photo" class="col-md-6 col-sm-6 col-xs-12">
                                          <i class="fa fa-warning" style="color:#f61f1f;"></i> <span style="font-size:12px;color:#f61f1f;">Maksimal ukuran Foto 600 kb</span>
                                            <input id="upload-review" type="file" name="file" />
                                        </div>
                                        <br>
                      </div>


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <input type="hidden" name='ids' value="<?php echo $ids;?>">
                        <input type="hidden" name='photo_old' value="<?php echo $photo_old;?>">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a class="btn btn-primary" href="<?php echo base_url();?>admin/events">Cancel</a>
						              <button class="btn btn-success" type="submit">Submit</button> 
                        </div>
                      </div>

                    </form>
                  </div>
                </div>

<script>
 
 $(document).ready(function() {
    $("#btn-upload-review").click(function() {
          $("#upload-review-photo").slideToggle("slow");
        });
        $('#uploadModal').on('hidden.bs.modal', function (e) {
          $( "#load-photo-review" ).load("<?php echo base_url();?>.'admin/galleri/ajax_get_review_photos?rvid='.$my_review_id)?>");
        })
        // enable fileuploader plugin
        $('#upload-review').fileuploader({
            limit: 1,
            extensions: ['PNG','JPG','JPEG','jpg', 'jpeg', 'png', 'gif'],
            changeInput: '<div class="fileuploader-input">' +
                              '<div class="fileuploader-input-inner">' +
                                  '<!--<img src="<?=base_url('_assets/global/img/fileuploader-dragdrop-icon.png')?>">-->' +
                                  '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                                  '<p>or</p>' +
                                  '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                              '</div>' +
                          '</div>',
            theme: 'dragdrop',
            upload: {
                url: '<?php echo base_url();?>admin/galleri/ajax_upload_photo',
                data: null,
                type: 'POST',
                enctype: 'multipart/form-data',
                start: true,
                synchron: true,
                beforeSend: null,
                onSuccess: function(result, item) {
                    var data = JSON.parse(result);
                    console.log(data);
                    // if success
                    if (data.isSuccess && data.files[0]) {
                        item.name = data.files[0].name;
                        item.html.find('.columns').append('<input type="hidden" name="photo[]" value="'+data.files[0].name+'"/>');
                    }
                    
                    // if warnings
                    if (data.hasWarnings) {
                        for (var warning in data.warnings) {
                            alert(data.warnings);
                        }
                        
                        item.html.removeClass('upload-successful').addClass('upload-failed');
                        // go out from success function by calling onError function
                        // in this case we have a animation there
                        // you can also response in PHP with 404
                        return this.onError ? this.onError(item) : null;
                    }
                    
                    item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                    setTimeout(function() {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);
                },
                onError: function(item) {
                    var progressBar = item.html.find('.progress-bar2');
                    
                    if(progressBar.length > 0) {
                        progressBar.find('span').html(0 + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
                        item.html.find('.progress-bar2').fadeOut(400);
                    }
                    
                    item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                        '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                    ) : null;
                },
                onProgress: function(data, item) {
                    var progressBar = item.html.find('.progress-bar2');
                    
                    if(progressBar.length > 0) {
                        progressBar.show();
                        progressBar.find('span').html(data.percentage + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                    }
                },
                onComplete: null,
            },
            onRemove: function(item) {
                $.post('<?php echo base_url();?>admin/galleri/ajax_remove_photo', {
                    file: item.name
                });
            },
            captions: {
                feedback: 'Drag and drop files here',
                feedback2: 'Drag and drop files here',
                drop: 'Drag and drop files here'
            }
        });
        
               
    });

</script>