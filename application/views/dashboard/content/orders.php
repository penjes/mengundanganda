 <div class="x_panel">
                  <div class="x_title">
                    <h2>Default Example <small>Users</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table class="table table-striped projects">
                      <thead>
                        <tr style="color:#777575;">
                          <th width="5%">No.</th>
                          <th>Nama Pasangan</th>
                          <th width="55%">Detail</th>
                          <th>Status</th>
                          <th></th>
                        </tr>
                      </thead>


                      <tbody>
                        <?php $no=$no++; 
                        if(!empty($data)) { 
                                foreach ($data as $key){ ?>
                                  <tr>
                                    <td><?php echo $no;?></td>
                                    <td>
                                      <?php echo $key->nama_link;?><br>
                                      <?php echo $key->mempelai_pria.' <i class="fa fa-heart"></i> '$key->mempelai_wanita;?><br>
                                      <i class="fa fa-calendar"></i>resepsi : <?php echo $key->acara_resepsi.'  '$key->start_at.' - '.$key->end_at;?><br>
                                      <?php echo '<i class="fa fa-mobile"></i>'.$key->telp.'  |   ';?>
                                      <?php echo '<img src="<?php echo base_url();?>assets/images/whatsapp.png" style="width:14px;">'.$key->whatsapp;?><br>
                                      <?php echo '<i class="fa fa-envelope"></i>'.$key->email;?>
                                    </td>
                                    <td><?php echo $key->reviews;?></td>
                                    <td></td>
                                  </tr> 
                      <?php       $no++;} 
                              }else{
                                echo "<tr><td colspan='5'><div class='alert alert-danger'>Data Kosong</div></tr>";
                              }
                        ?>
                      </tbody>
                    </table>
                    <?php if(!empty($pagination)){?>
                    <div class="col-md-6 text-right">
                        <?php echo $pagination ?>
                    </div>
                    <?php } ?>
                  </div>
                </div>