<!DOCTYPE html>
<html lang="en">
<?php 
  if(empty($data)){
    $data='';
  }
  $this->load->view('dashboard/partial/head',$data);
?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       <?php 
            $this->load->view('dashboard/partial/navbar',$data);
            $this->load->view('dashboard/partial/top_navigation',$data);  
       ?>

        
        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <?php $this->load->view('dashboard/content/'.$page,$data); ?>
          <!-- /top tiles -->

           
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <?php $this->load->view('dashboard/partial/script',$data);?>
  </body>
</html>
