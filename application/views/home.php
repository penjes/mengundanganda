<?php 
//echo $is_page[0]['head_title'];
//var_dump($is_page);

//die();?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo ucfirst($biodata->mempelai_pria);?> &amp; <?php echo ucfirst($biodata->mempelai_wanita);?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo ucfirst($biodata->mempelai_pria);?> &amp; <?php echo ucfirst($biodata->mempelai_wanita);?> mengundang anda" />
	<meta name="keywords" content="Datang di acara kami" />
	<meta name="author" content="mengundanganda.com" />

    <style type="text/css">
    	#name{
    		padding:10px !important;
    	}
   	</style>

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="<?php echo ucfirst($biodata->mempelai_pria);?> &amp; <?php echo ucfirst($biodata->mempelai_wanita);?> mengundang anda"/>
	<meta property="og:image" content="<?php echo base_url();?>assets/images/bg/slider.jpg"/>
	<meta property="og:url" content="https://mengundanganda.com"/>
	<meta property="og:site_name" content="mengundanganda"/>
	<meta property="og:description" content="<?php echo ucfirst($biodata->mempelai_pria);?> &amp; <?php echo ucfirst($biodata->mempelai_wanita);?> We Are Getting Married <?php echo $biodata->acara_resepsi; ?>"/>
	<meta name="twitter:title" content="<?php echo ucfirst($biodata->mempelai_pria);?> &amp; <?php echo ucfirst($biodata->mempelai_wanita);?> mengundang anda" />
	<meta name="twitter:image" content="<?php echo image_src_front($biodata->bg,'bg');?>" />
	<meta name="twitter:url" content="https://mengundanganda.com" />
	<meta name="twitter:card" content="<?php echo ucfirst($biodata->mempelai_pria);?> &amp; <?php echo ucfirst($biodata->mempelai_wanita);?> We Are Getting Married <?php echo $biodata->acara_resepsi; ?>" />

	<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
	
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/images/favicon.png">	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url();?>assets/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="<?php echo base_url();?>assets/js/respond.min.js"></script>
	<![endif]-->

	<!-- jQuery -->
	  
	</head>
	<body>
		
	<div class="fh5co-loader"></div>
	
	<div id="page">
	<nav class="fh5co-nav" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-xs-2">
					<div id="fh5co-logo">
						<a href="#">Wedding<strong>.</strong></a></div>
				</div>
				<div class="col-xs-10 text-right menu-1">
					<ul class="jetmenu" id="jetmenu">
						<li class="active">
							<a href="#home">Home</a>
						</li>
						<li><a href="#our-wedding">Our Wedding</a></li>
						<li>
							<a href="#event">Event</a>
						</li>
						<li><a href="#story">Story</a></li>
						<li>
							<a href="#galleri">Galleri</a>
						</li>
						<li>
							<a href="#testimoni">Review</a>
						</li>
						<li>
							<a href="#ucapan">Are You Attending?</a>
						</li>
						
					</ul>
				</div>
			</div>
			
		</div>
	</nav>

	<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(assets/images/bg/slider.jpg);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container" id="home">
			<div class="row">

				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<center>
								<h1 class="f-55"><?php echo ucfirst($biodata->mempelai_pria);?> <br>
									&amp; <br>
									<?php echo ucfirst($biodata->mempelai_wanita);?></h1>
							</center>
							<h2>Akan Melangsungkan Pernikahan <?php echo $notif;?></h2>
							<?php if($notif=='y'){?>
							<div class="simply-countdown simply-countdown-one"></div>
							<?php }else{ ?>
									<div class="simply-countdown">
										<div class="simply-section simply-days-section">
										<div>
										<span class="simply-amount"><?php echo $biodata->tgl;?></span>
									<span class="simply-word">Date</span>
								</div>
								</div>
									<div class="simply-section simply-hours-section">
										<div>
											<span class="simply-amount"><?php echo $biodata->bln;?></span>
											<span class="simply-word">Month</span>
									</div>
									</div>
										<div class="simply-section simply-minutes-section">
											<div>
											<span class="simply-amount"><?php echo $biodata->thn;?></span>
										<span class="simply-word">Year</span>
									</div>
									</div>
									 
							</div>

							<?php } ?>

							<p><a href="#" class="btn btn-default btn-sm">Save the date</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="fh5co-couple">
		<div class="container" id="our-wedding">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<h2><?php echo $is_page[0]['head_title'];?></h2>
					<h3><?php echo $is_page[0]['title'];?></h3>
					<p><?php echo $is_page[0]['content'];?></p>
				</div>
			</div>
			<div class="couple-wrap animate-box">
				<div class="couple-half">
					<div class="groom">
						<img src="<?php echo image_src_front($biodata->photo_pria,'manten');?>" alt="groom" class="img-responsive">
					</div>
					<div class="desc-groom">
						<h3><?php echo ucfirst($biodata->mempelai_pria);?></h3>
						<p><?php echo ucfirst($biodata->content_pria);?></p>
					</div>
				</div>
				<p class="heart text-center"><i class="icon-heart2"></i></p>
				<div class="couple-half">
					<div class="bride">
						<img src="<?php echo image_src_front($biodata->photo_wanita,'manten');?>" alt="groom" class="img-responsive">
					</div>
					<div class="desc-bride">
						<h3><?php echo ucfirst($biodata->mempelai_wanita);?></h3>
						<p><?php echo ucfirst($biodata->content_wanita);?></p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-event" class="fh5co-bg" style="background-image:url(assets/images/is_page/<?php echo $is_page[1]['image'];?>);">
		<div class="overlay"></div>
		<div class="container" id="event">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<span><?php echo $is_page[1]['head_title'];?></span>
					<h2><?php echo $is_page[1]['title'];?></h2>
				</div>
			</div>
			<div class="row">
				<div class="display-t">
					<div class="display-tc">
						<div class="col-md-10 col-md-offset-1">
							<?php 
							$col = 12/$tot_wed;
							foreach ($wedding_event as $key) { ?>
							<div class="col-md-<?php echo $col;?> col-sm-<?php echo $col;?> text-center">
								<div class="event-wrap animate-box">
									<h3><?php echo $key->title;?></h3>
									<div class="event-col">
										<i class="icon-clock"></i>
										<span><?php echo $key->start_at;?></span>
										<span><?php echo $key->end_at;?></span>
									</div>
									<div class="event-col">
										<i class="icon-calendar"></i>
										<span><?php echo $key->isdate;?></span>
										<span><?php echo $key->ismonth;?></span>
									</div>
									<p><?php echo $key->description;?></p>
								</div>
							</div>
							<?php  } ?> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php if(!empty($our_story)){?>
	<div id="fh5co-couple-story">
		<div class="container" id="story">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<span><?php echo $is_page[2]['head_title'];?></span>
					<h2><?php echo $is_page[2]['title'];?></h2>
					<p><?php echo $is_page[2]['head_title'];?></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-md-offset-0">
					<ul class="timeline animate-box">
						<?php 
						$no=1;
						foreach ($our_story as $key){ ?>
						<li class="<?php if($no%2 != 0){ echo 'animate-box';}else{ echo 'timeline-inverted animate-box'; }?>">
							<div class="timeline-badge" style="background-image:url(<?php echo image_src_front($key->image,'story');?>);"></div>
							<div class="timeline-panel">
								<div class="timeline-heading">
									<h3 class="timeline-title"><?php echo $key->title;?></h3>
									<?php if(!empty($key->tampilkan_date_history)){?>
									<span class="date"><?php echo $key->history_date;?></span>
									<?php } ?>
								</div>
								<div class="timeline-body">
									<p><?php echo $key->content;?></p>
								</div>
							</div>
						</li> 
						<?php $no++;} ?>
			    	</ul>
				</div>
			</div>
		</div>
	</div>
	<?php } 
	if(!empty($galleri)){?>
	<div id="fh5co-gallery" class="fh5co-section-gray">
		<div class="container" id="galleri">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<span><?php echo $is_page[3]['head_title'];?></span>
					<h2><?php echo $is_page[3]['title'];?></h2>
					<p><?php echo $is_page[3]['head_title'];?></p>
				</div>
			</div>
			<div class="row row-bottom-padded-md">
				<div class="col-md-12">
					<ul id="fh5co-gallery-list">
						<?php foreach ($galleri as $key){?>
						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(<?php echo image_src_front($key->image,'galleri');?>); "> 
						<a href="#">
							<div class="case-studies-summary">
								<span><?php echo $key->title;?></span>
								<h2><?php echo $key->subtitle;?></h2>
							</div>
						</a>
					</li> 
						<?php } ?>
					</ul>		
				</div>
			</div>
		</div>
	</div>
<?php } ?>
	
	 <div id="fh5co-counter" class="fh5co-bg fh5co-counter" style="background-image:url(assets/images/is_page/<?php echo $is_page[4]['image'];?>);">
		<div class="overlay"></div>
		<div class="container" style="color:#fff;">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					 <h2><?php echo $is_page[4]['title'];?></h2>
					<p><?php echo $is_page[4]['head_title'];?></p>
				</div>
			</div>

			<div class="row animate-box">
				<div class="col-md-10 col-md-offset-1">
					<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<?php echo $is_page[4]['content'];?>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
 

	<?php if((!empty($reviews))&&($tot_rev > 1)){?>
	<div id="fh5co-testimonial">
		<div class="container" id="testimoni">
			<div class="row">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
						 
					<span><?php echo $is_page[4]['head_title'];?></span>
					<h2><?php echo $is_page[4]['title'];?></h2> 
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 animate-box">
						<div class="wrap-testimony">
							<div class="owl-carousel-fullwidth">
								<?php foreach ($reviews as $key){ ?>								
								<div class="item">
									<div class="testimony-slide active text-center">
										<figure>
											<img src="<?php echo base_url();?>assets/images/family2.jpg" alt="user">
										</figure>
										<span><?php echo ucfirst($key->nama);?></span>
										<blockquote>
											<p>"<?php echo ucfirst($key->reviews);?>"</p>
										</blockquote>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

	 

	<div id="fh5co-started" class="fh5co-bg" style="background-image:url(assets/images/is_page/<?php echo $is_page[6]['image'];?>);">
		<div class="overlay"></div>
		<div class="container" id="ucapan">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					 <h2><?php echo $is_page[6]['title'];?></h2>
					<p><?php echo $is_page[6]['head_title'];?></p>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-10 col-md-offset-1">
					<div style="margin-top: 8px" id="message">
                    	<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                	</div>
					<form class="form-inline" action="<?php echo base_url();?>home/send" method="post">

						<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<label for="name" class="sr-only">Nama</label>
								<input type="text" class="form-control" name="nama" autocomplete="off" id="name" placeholder="Nama" required>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<label for="email" class="sr-only">Kartu Ucapan</label>
								<textarea class="form-control" autocomplete="off" id="kartu" name="kartu" placeholder="Kartu Ucapan" required></textarea>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 center">
							<div class="col-md-4 col-sm-4">
							</div>
							<div class="col-md-4 col-sm-4">
								<button type="submit" class="btn btn-default btn-block">Kirim</button>
							</div>
							<div class="col-md-4 col-sm-4">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<footer id="fh5co-footer" role="contentinfo">
		<div class="container">

			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; <?php echo date('Y');?> All Rights Reserved.</small> 
						<small class="block">Present by <a href="#" target="_blank">mengundanganda.com</a></small>
					</p>
					<p>
						<ul class="fh5co-social-icons">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-linkedin"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
					</p>
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop">
			<i class="icon-arrow-up"></i></a>
	</div>
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	
	<!-- jQuery Easing -->
	<script src="<?php echo base_url();?>assets/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url();?>assets/js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="<?php echo base_url();?>assets/js/jquery.countTo.js"></script>

	<!-- Stellar -->
	<script src="<?php echo base_url();?>assets/js/jquery.stellar.min.js"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/magnific-popup-options.js"></script>

	<!-- // <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.js"></script> -->
	<script src="<?php echo base_url();?>assets/js/simplyCountdown.js"></script>
	<!-- Main -->
	<script src="<?php echo base_url();?>assets/js/main.js"></script>

	<script>
    //var d = new Date(new Date().getTime() + 200 * 120 * 120 * 2000);
    var d = new Date('<?php echo $biodata->acara;?>');
    console.log('var d : '+d);
    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });
</script>

	</body>
</html>


