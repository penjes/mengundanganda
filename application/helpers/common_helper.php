<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function image_src($image,$folder){
	$filename= FCPATH . "assets/images/".$folder."/" . $image;
	if(!empty($image)){
		if (file_exists($filename)) {
			 return base_url("assets/images/".$folder."/{$image}");
		}else{
			 return base_url("assets/images/default.PNG");
		}
	}else{
			 return base_url("assets/images/default.PNG");
	}
}

function image_src_front($image,$folder){
	$filename= FCPATH . "assets/images/".$folder."/" . $image;
	if(!empty($image)){
		if (file_exists($filename)) {
			 return base_url("assets/images/".$folder."/{$image}");
		}else{
			 return base_url("assets/images/couple-3.JPG");
		}
	}else{
			 return base_url("assets/images/couple-3.JPG");
	}
}


function pecah_digit_3($kata){
	$gabung='';
	for ($i=0; $i < strlen($kata) ; $i++) { 
		if($i==0){
			$gabung = $gabung.$kata[$i];
		}
		else if($i%3 == 0 ){
			$gabung = $gabung.' . '.$kata[$i];
		}else{
			$gabung = $gabung.$kata[$i];
		}
	}
	return $gabung;
}

function only_image_src($image,$folder){
	$filename= FCPATH . "assets/images/".$folder."/" . $image;
	if(!empty($image)){
		if (file_exists($filename)) {
			 return $image;
		}else{
			 return "default.jpg";
		}
	}else{
			 return "default.jpg";
	}
}

if (!function_exists('_template_dashboard')) {
	  function _template_dashboard($view="",$data= array()){
		 	// $ci =& get_instance();
			$this->load->view('admin/partials/top');
			$this->load->view($view,$data);
			$this->load->view('admin/partials/bottom');
		}

}

function log_in_user(){
	if ((empty($this->session->userdata('users')))&&(empty($this->session->userdata('users')))) {
		redirect('admin/login');
	} 
}

function encode_id($id){
	$generate_id = "mengundangandauser".$id;
	return  base64_encode(base64_encode(base64_encode($generate_id))); 
}

function decode_id($id){
	$generate_id = base64_decode(base64_decode(base64_decode($id)));
	return str_replace('mengundangandauser', '', $generate_id);
}
	function generate(){
	  $int = rand(0,51);
	  $int2 = rand(0,51);
	  $one=rand(0,9);
	  $int3=rand(0,20);
	  $int4=rand(100,400);
	  $null = $one.'00000';
	  $cek =$int3.$int4;
	  $null = $null + $cek;
	  $rand_letter =$null;
	  return $rand_letter;
	}

	function noreply_mail($to,$subject,$content){
		$ci = get_instance();
        $ci->load->library('email'); 

        $config['protocol'] = "smtp.gmail.com";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user']    = 'noreplymracangmarket@gmail.com';
		$config['smtp_pass']    = 'mracangmarket';      
        $config['charset'] = "utf-8";
        $config['validation'] = TRUE;
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        
        $ci->email->initialize($config);
        $ci->email->clear(TRUE);
        $ci->email->from('noreplymracangmarket@gmail.com', $subject);
        $ci->email->to($to);
        $ci->email->subject($subject);
        $ci->email->message($content);
        // $ci->email->attach($pdffilelocation);
        if ($ci->email->send()) {
					return true;
				}
	}



	function template_email($template=null,$user=null,$kode=null,$order=null){
		$this->db->where('email',$user);
		$emailcek = $this->db->get('users')->row();

		$con=$this->db->get('configuration')->row();


		$this->db->limit(1);
		$bank=$this->db->get('rekening_q')->row();
		
		$this->db->where('template_name',$template);
		$content_email = $this->db->get('email_template')->row_array();
		if(!empty($order)){
			$this->db->select("
								`user_id`,
								 `nama_bank`,
								 `transfer_ke_rek`,
								 `transfer_dari`,
								 `shipping`,
								 `ekspedisi`,
								 `discon`,
								 `cashback`,
								 `status_kupon`,
								 `kode_kupon`,
								 `total_satuan`,
								 `total_tagihan`,
								 `total_bayar`,
								 `kode_transaksi`,
								 `created_at`,
								 DATE_FORMAT(`date_expired`,'%d %M %Y %H:%i %p')as date_expired,
								 `date_transfer`,
								 `date_hold`,
								 `date_confirm`,
								 `note_user`,
								 `no_resi`,
								 `remark`,
								 `status`
							");
			$this->db->where('status','pending');
			$this->db->where('user_id',$order);
			$order_temp=$this->db->get('order_temp')->row();
		}
		if(!empty($order_temp)){
		$this->db->select('id,
                           id_user,
                           name_product,
                           order_id,
                           (select photo from photo_product where product_id=order_detail.product_id limit 1)as image,
                           product_id,
                           shipping,
                           cashback,
                           jumlah,harga,
                           harga_per_pcs, 
                           DATE_FORMAT(created_at,"%d %M %Y %H:%i %p")as created_at,
                           status');
		$this->db->where('order_id',$order_temp->id);
		$detail=$this->db->get('order_detail')->result();

		}
					 if(!empty($content_email)){

					 $this->db->where('id_email',$content_email['id']);
					 $this->db->where('position','header');
					 $photo_h = $this->db->get('email_photo')->row();


					 $this->db->where('id_email',$content_email['id']);
					 $this->db->where('position','header');
					 $photo_f = $this->db->get('email_photo')->row();

					 $link = base_url();
					 $is_content = $content_email['description'];
					 if(!empty($photo_h)){
					 	$header_photo = '<img class="CToWUd" src="'.base_url().'/assets/images/users/'.$photo_h->photo.'" xss=removed>';
					 }else{
					 	$header_photo = "";
					 }
					 if(!empty($photo_f)){
					 	$footer_photo = '<img class="CToWUd" src="'.base_url().'/assets/images/users/'.$photo_f->photo.'" xss=removed>';
					 }else{
					 	$footer_photo="";
					 }
					 //---- detail user
					 $is_content = str_replace('%%NAME_USER%%',$emailcek->nama,$is_content);
					 $is_content = str_replace('%%NAME_PENERIMA%%',$emailcek->nama,$is_content);
					 $is_content = str_replace('%%ADDRESS_PENERIMA%%',$emailcek->address,$is_content);
					 $is_content = str_replace('%%KECAMATAN%%',$emailcek->nama,$is_content);
					 $is_content = str_replace('%%KOTA%%',$emailcek->nama,$is_content);
					 $is_content = str_replace('%%PROVINSI%%',$emailcek->nama,$is_content);
					 $is_content = str_replace('%%TELP%%',$emailcek->nama,$is_content);
					// $is_content = str_replace('%?SE_URL_AKTIFASI_AKUN%%',$emailcek->activation_code,$is_content);

					 $is_content = str_replace('%%OLSHOP%%',$con->title,$is_content);
					 $is_content = str_replace('%%BASE_URL%%',$link,$is_content);
					 $is_content = str_replace('%?SE_URL%%',$link,$is_content);
					 $is_content = str_replace('%%ICON_IMAGE%%',$header_photo,$is_content);
					 $is_content = str_replace('%%KODE%%',$kode,$is_content);
					 $is_content = str_replace('%%ICON_FOOTER%%',$footer_photo,$is_content);
					 $is_content = str_replace('%%TAHUN%%',date("Y"),$is_content);
					 //------- order template
					 if(!empty($order_temp)){

						 $is_content = str_replace('%%KODE_TRANSAKSI%%',$order_temp->kode_transaksi,$is_content);
						 $is_content = str_replace('%%TOTAL_TAGIHAN%%',$order_temp->total_tagihan,$is_content);
						 
					 }
					 $is_content = str_replace('%%LOGO_BANK%%',$bank->photo,$is_content);
					 $is_content = str_replace('%%NAME_BANK%%',$bank->nama_bank,$is_content);
					 $is_content = str_replace('%%NO_REKENING%%',$bank->kode_rekening,$is_content);
					 $order_content='';
					 if(!empty($detail)){
					 	foreach ($detail as $key) {
						 	$order_content = $order_content.'<tr><td><div><img alt="" class="CToWUd" src="'.image_src($key->image,'produk').'" /></div></td><td><strong>'.$key->name_product.' </strong><p>&nbsp;</p>	<strong> </strong></td><td>&nbsp;</td>';
						 }

					 $is_content = str_replace('%%ORDER_DETAIL%%',$order_content,$is_content);
					 }
					 
					  
					 /* 
						%%BASE_URL_AKTIFASI_AKUN%%

					 %%ORDER DETAIL%%
							<tr>
												<td>
												<div class="m_1077991220476759385img"><a href="#" ><img alt="" class="CToWUd" src="%%PHOTO_PRODUCT%%" /> </a></div>
												</td>
												<td><strong>%%PRODUCT_NAME%% </strong><p>&nbsp;</p>	<strong> </strong></td>
												<td>&nbsp;</td>
											</tr>
					 */
					//  var_dump($is_content);die();
					 $content = $is_content;
					}else{
						$content='';
					}
					return $content;
				

	}