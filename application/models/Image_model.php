<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Image_model extends CI_Model
{

function add($arr){
	$this->db->set('created_at','now()',false);
	$this->db->insert('image_temp',$arr);
}

function delete($name){
	$this->db->where('photo',$name);
	$this->db->delete('image_temp');
	// var_dump($this->db->last_query());die();
}


//--- for slder -----------------------------------
function get_slider_home($where="",$orderby=""){
	if (!empty($where)) {
		$this->db->where($where);
	}
	$this->db->order_by($orderby);
	$data=$this->db->get('slider');
	return $data->result();
}

function get_slider_custom($where=""){
	if (!empty($where)) {
		$this->db->where($where);
	}
	$data=$this->db->get('slider');
	return $data;
}
function get_slider($where="",$page=0,$limit=0,$order_by="id desc"){
		if (!empty($where)) {
		$this->db->like('status',$where);
		$this->db->or_like('position',$where);
		
		}
		if (intval($limit) > 0) {
		$this->db->limit($limit,$page);
		}
		$this->db->order_by($order_by);
		$this->db->select('SQL_CALC_FOUND_ROWS *',false);
		$data=$this->db->get('slider');
		// var_dump($this->db->last_query());
		return $data;
	}

	function get_search($id){
		$this->db->where('id',$id);
		$data=$this->db->get('slider')->row();
		return $data;
	}


function delete_slider($id){
		  $this->db->where('id',$id);
	$data=$this->db->get('slider')->row();
	if (!empty($data)) { 
			$filename= FCPATH . "assets/images/slider/" . $data->photo;
			if (file_exists($filename)) {
				 unlink($filename);
			} 
	}
		$this->db->where('id',$id);
		$log=$this->db->delete('slider'); 
		return $log;
}


	function add_photo_slider($arr,$id){		
		$this->db->set('created_at','now()',false);
		$data=$this->db->insert('slider',$arr);
		return $data;

	}
	function update_photo_logo($arr){
	$log=$this->db->update('configuration',$arr);
	return $log;
	}

	function update_photo_headerPage($arr,$id){
		$this->db->where('id',$id);
		$log=$this->db->update('headerPage',$arr);
		return $log;
		}


function update_photo($id,$arr){
	$this->db->where('id',$id);
	$log=$this->db->update('slider',$arr);
	// var_dump($this->db->last_query());die();
	return $log;
}
public function get_total_slider($where="",$page=0,$limit=0,$order_by="id desc")
    {
        if (!empty($where)) {
		$this->db->like('status',$where);
		$this->db->or_like('position',$where);
		
		}
		$this->db->order_by($order_by);
		$this->db->select('SQL_CALC_FOUND_ROWS *',false);
		$data=$this->db->get('slider');
		// var_dump($this->db->last_query());
		return $data->num_rows();
    }

}