<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {
  function __construct()
    {
        parent::__construct();
           $this->load->library('image_lib');

        if ((empty($this->session->userdata('users')))&&(empty($this->session->userdata('users')))) {
            redirect('admin/login');
        }
         
    }
   
	 
	public function index()
	{
    redirect('admin/configuration');
	}
	 

	 
public function ajax_upload_photo()
  {
     
    // initialize FileUploader
    $FileUploader = new FileUploader('file', array(
        'limit' => 1,
        'maxSize' => null,
        'fileMaxSize' => 8,
        'extensions' => ['PNG','JPG','JPEG','jpg', 'jpeg', 'png', 'gif'],
        'required' => false,
        'uploadDir' => FCPATH.'assets/images/bg/',
        'title' => 'photo_{random}_{timestamp}',
        'replace' => false,
        'listInput' => false,
        'files' => null
    ));
  
    // call to upload the files
    $data = $FileUploader->upload();
    $arr = array('photo'=>$data['files'][0]['name']);
    $this->db->set('created_at','NOW()',false);
    $this->db->insert('image_temp',$arr);
    echo json_encode($data);
    exit;
  }

  public function addslider(){
    
    $data = array(  'ids' =>'',
                    'photo_old' => ''
          );
     
    $data['page']='slider_upload';
    $data['action'] = 'add_photo';
    $this->load->view('dashboard/template',$data);
  }


   
	   

	public function edit($id){
		$this->db->where('id',decode_id($id));
		$key=$this->db->get('konfiguration')->row();
	
		$data = array(	'ids' =>encode_id($key->id),
						        'photo_old' => $key->bg 
					);
		 
		$data['page']='slider_upload';
		$data['action'] = 'ubah_photo';
		$this->load->view('dashboard/template',$data);
	}

  public function rubah($id){
    $this->db->where('id',decode_id($id));
    $key=$this->db->get('slider')->row();
  
    $data = array(  'ids' =>encode_id($key->id),
                    'photo_old' => $key->image 
          );
     
    $data['page']='slider_upload';
    $data['action'] = 'ubah_photo_slider';
    $this->load->view('dashboard/template',$data);
  }




  function add_photo(){

     if ((sizeof($_POST) > 0)) {
      $photo = NULL;
            $arr = NULL;
              foreach($_POST as $key => $val)
              {
                if(!is_array($val))
                { 
                  if ($key !='fileuploader-list-file') {
                      if ($key !='id') {
                           $arr[$key] = trim($val); 
                     }
                  }
                 
                }
              }
     
          if(!empty($_POST['photo'])){
            for ($i=0; $i <  count($_POST['photo']) ; $i++) {
                      if (!empty($_POST['photo'][$i])) {
                      $this->db->where('photo',trim($_POST['photo'][$i]));
                      $this->db->delete('image_temp');
                      $photo = trim($_POST['photo'][$i]); 
                      }
              }
          }
            if(!empty($photo)){
              //------ compress -------------
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/images/bg/'.$photo;
                $config['maintain_ratio'] = TRUE;
                $config['width']     = 900;
                $config['height']   = 600;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
              //-----------------------------
              } 

            if($_SESSION['user_level']==1){
              $kode_internal = $_SESSION['kode_mirror_internal'];
            }else{
              $kode_internal = $_SESSION['kode_internal'];
            }  

            $arr =array('image'=>trim($photo),'kode_internal'=>$kode_internal,'status'=>'1','update_from'=>$_SESSION['users']);
            $this->db->set('created_at','NOW()',false);
            $this->db->set('last_update','NOW()',false);
            $log=$this->db->insert('slider',$arr);
                      

           if ($log) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success">File berhasil di tambahkan</div>');
           }else{
            $this->session->set_flashdata('msg', '<div class="alert alert-danger">File gagal ditambahkan</div>');
           }
           redirect('admin/configuration');
           exit();
        }
         $this->session->set_flashdata('msg', '<div class="alert alert-danger">Foto gagal ditambahkan</div>');
         redirect('admin/configuration');


  }
  // ubah foto slider

  function ubah_photo_slider(){

    //security
              if($_SESSION['user_level']==1){
                  $kode_internal = $_SESSION['kode_mirror_internal'];
              }else{
                  $kode_internal = $_SESSION['kode_internal'];
              }

            $this->db->where('kode_internal',$kode_internal);
            $this->db->where('id',decode_id($_POST['ids']));
            $log1=$this->db->get('slider')->row();
            // var_dump($this->db->last_query());

            // die();
          

           if ($log1) {
           
           }else{
            $this->session->set_flashdata('msg', '<div class="alert alert-danger">Gagal Upload File</div>');
            redirect('admin/configuration');
           exit();
           }
           

           // echo 'photo nya : '.$photo_old;
           //  var_dump($_POST);

           //  die();

     if ((sizeof($_POST) > 0)) {
      $photo = NULL;
            $arr = NULL;
              foreach($_POST as $key => $val)
              {
                if(!is_array($val))
                { 
                  if ($key !='fileuploader-list-file') {
                if ($key !='id') {
                     $arr[$key] = trim($val); 
               }
                  }
                 
                }
              }
     
          if(!empty($_POST['photo'])){
            for ($i=0; $i <  count($_POST['photo']) ; $i++) {
                      if (!empty($_POST['photo'][$i])) {
                      $this->db->where('photo',trim($_POST['photo'][$i]));
                      $this->db->delete('image_temp');
                      $photo = trim($_POST['photo'][$i]); 
                      }
                      
              }
          }
              
            $photo_old = $_POST['photo_old'];
            // echo 'photo nya : '.$photo_old;
            // var_dump($_POST);

            // die();
            if(!empty($photo)){
              // echo 'masuk';
              //  die();
                if(!empty($photo_old)){   
                    if($photo !== $photo_old){
                  
                          $file = FCPATH.'assets/images/bg/'.$photo_old;// hapus file di server             
                                        if(file_exists($file)){
                                          unlink($file);
                                        }
                                        //------ compress -------------
                                        $config['image_library'] = 'gd2';
                                        $config['source_image'] = 'assets/images/bg/'.$photo;
                                        $config['maintain_ratio'] = TRUE;
                                        $config['width']     = 900;
                                        $config['height']   = 600;
                                        $this->image_lib->initialize($config);
                                        $this->image_lib->resize();
                                        //-----------------------------
                    }else{
                      $photo = $photo_old;
                    }
                }
              }else{
                      $photo = $photo_old;
                    }
            $arr =array('image'=>trim($photo),'update_from'=>$_SESSION['users']);
            
             if($_SESSION['user_level']==1){
                  $kode_internal = $_SESSION['kode_mirror_internal'];
              }else{
                  $kode_internal = $_SESSION['kode_internal'];
              }

            $this->db->where('kode_internal',$kode_internal);
            $this->db->where('id',decode_id($_POST['ids']));
            $this->db->set('last_update','NOW()',false);
            $log=$this->db->update('slider',$arr);
                      

           if ($log) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success">File berhasil di tambahkan</div>');
           }else{
            $this->session->set_flashdata('msg', '<div class="alert alert-danger">File gagal ditambahkan</div>');
           }
           redirect('admin/configuration');
           exit();
        }
         $this->session->set_flashdata('msg', '<div class="alert alert-danger">Foto gagal ditambahkan</div>');
         redirect('admin/configuration');


  }





  // end ubah foto slider


	function ubah_photo(){

  	 if ((sizeof($_POST) > 0)) {
  	 	$photo = NULL;
            $arr = NULL;
              foreach($_POST as $key => $val)
              {
                if(!is_array($val))
                { 
                  if ($key !='fileuploader-list-file') {
                if ($key !='id') {
                     $arr[$key] = trim($val); 
               }
                  }
                 
                }
              }
  	 
        	if(!empty($_POST['photo'])){
        		for ($i=0; $i <  count($_POST['photo']) ; $i++) {
                      if (!empty($_POST['photo'][$i])) {
	                    $this->db->where('photo',trim($_POST['photo'][$i]));
	                    $this->db->delete('image_temp');
	                    $photo = trim($_POST['photo'][$i]); 
                      }
                      
              }
        	}
              
            $photo_old = $_POST['photo_old'];
            // echo 'photo nya : '.$photo_old;
            // // var_dump($_POST);

            // die();
            if(!empty($photo)){
              // echo 'masuk';
              //  die();
                if(!empty($photo_old)){		
                		if($photo !== $photo_old){
                	
          								$file = FCPATH.'assets/images/bg/'.$photo_old;// hapus file di server             
          		                          if(file_exists($file)){
          		                            unlink($file);
          		                          }
          		                          //------ compress -------------
          		                          $config['image_library'] = 'gd2';
          		                          $config['source_image'] = 'assets/images/bg/'.$photo;
          		                          $config['maintain_ratio'] = TRUE;
          		                          $config['width']     = 900;
          		                          $config['height']   = 600;
          		                          $this->image_lib->initialize($config);
          		                          $this->image_lib->resize();
          		                          //-----------------------------
          					}else{
          						$photo = $photo_old;
          					}
                }
              }else{
                      $photo = $photo_old;
                    }
            $arr =array('bg'=>trim($photo));
                      
            $this->db->where('id',decode_id($_POST['ids']));
            $log=$this->db->update('konfiguration',$arr);
                      

           if ($log) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success">File berhasil di tambahkan</div>');
           }else{
            $this->session->set_flashdata('msg', '<div class="alert alert-danger">File gagal ditambahkan</div>');
           }
           redirect('admin/configuration');
           exit();
        }
         $this->session->set_flashdata('msg', '<div class="alert alert-danger">Foto gagal ditambahkan</div>');
         redirect('admin/configuration');


	}



public function ajax_remove_photo()
              {
                $filename = $_POST['file'];
                echo $filename;
                if (isset($filename)) {
                    $file = FCPATH.'assets/images/bg/'.$filename;
                    if(file_exists($file))
                    unlink($file);
                }
              }


public function hapus($id=null)
              {
                $this->db->where('id',decode_id($id));
                $data=$this->db->get('slider')->row();

                $filename = $data->image;
                echo $filename;
                if (isset($filename)) {
                    $file = FCPATH.'assets/images/bg/'.$filename;
                    if(file_exists($file))
                    unlink($file);
                }

                 if($_SESSION['user_level']==1){
                  $kode_internal = $_SESSION['kode_mirror_internal'];
              }else{
                  $kode_internal = $_SESSION['kode_internal'];
              }

                $this->db->where('kode_internal',$kode_internal);
                $this->db->where('id',decode_id($id));
                $data=$this->db->delete('slider');
                if ($log) {
                  $this->session->set_flashdata('msg', '<div class="alert alert-success">File berhasil di hapus</div>');
                 }else{
                  $this->session->set_flashdata('msg', '<div class="alert alert-danger">File gagal dihapus</div>');
                 }
                 redirect('admin/configuration');
                 exit();
              }


}
