<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');        
	    $this->fpath  = "admin/";
	    $this->load->helper('common_helper');
    
    }
	 

    function index(){
    	$this->listing(0);
    }



	public function listing($page=0)
	{
		
		$limit =25;
		// $cari = array('expired','order','payment','confirm');
		if((isset($_GET['search']))&&(empty($_GET['search']))){
			$this->db->like('mempelai_pria',$_GET['search']);
			$this->db->or_like('mempelai_wanita',$_GET['search']);
		}
		$this->db->limit($limit,$page);
		if((isset($_GET['status']))&&(!empty($_GET['status']))){
			$this->db->wherein('status',$_GET['status']);
		}		
		$data['orders_undangan'] = $this->db->get('orders_undangan')->result();
		$total_row = $this->db->get("orders_undangan")->num_rows();
	    $config["base_url"] = base_url()."admin/orders/listing";
	    $config["uri_segment"] = 3;
	    if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
	    $config['full_tag_open'] = '<ul class="pagination" id="pagenumber-list-ajax">';
	    $config['full_tag_close'] = '</ul>';
	    $config['first_link'] = '';
	    $config['first_tag_open'] = '';
	    $config['first_tag_close'] = '';
	    $config['last_link'] = '';
	    $config['last_tag_open'] = '';
	    $config['last_tag_close'] = '';
	    $config['next_link'] = 'Next &raquo;';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['prev_link'] = '&laquo; Prev';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    // $config['num_links'] = $total_row;
	    // $config['use_page_numbers'] = FALSE;
	    $config["total_rows"] = $total_row;
	    $data['no']=$page;
	    $this->pagination->initialize($config);
	    $data["pagination"] = $this->pagination->create_links();
	    $data['page']='orders';
		$this->load->view('dashboard/template',$data);
	}

	function confirmasi($id=null){
		if(!empty($id)){
			$this->db->where('id',decode_id($id));
			$data=$this->db->get('orders_undangan')->row();

			if(!empty($data)){
				$arr = array('paket'=>$data->paket,
							 'is_template'=>$data->is_template,
							 'mempelai_pria'=>$data->mempelai_pria,
							 'mempelai_wanita'=>$data->mempelai_wanita,
							 'ayah_pria'=>$data->ayah_pria,
							 'ibu_pria'=>$data->ibu_pria,
							 'ayah_wanita'=>$data->ayah_wanita,
							 'ibu_wanita'=>$data->ibu_wanita,
							 'acara_resepsi'=>$data->acara_resepsi,
							 'start_at'=>$data->start_at,
							 'end_at'=>$data->end_at,
							 'telp'=>$data->telp,
							 'whatsapp'=>$data->whatsapp,
							 'email'=>$data->email,
							 'status'=>'1',
							 'expired_date'=> (date('Y-m-d H:i:s', strtotime('+1 years')))
							);

				$this->db->set('crated_at','NOW()',false);
				$log=$this->db->insert('konfiguration',$arr);


				$arrs = array('confirm_from'=>$_SESSION['users']);


				if($log){
						$this->db->where('id',decode_id($id));
						$this->db->set('confirm_at','NOW()',false);
						$this->db->update('orders_undangan',$arrs);

					$this->session->set_flashdata('msg', '<div class="alert alert-success">Pesanan " '.$data->kode_pesanan.' " telah berhasil di proses</div>');
					
				}else{
					$this->session->set_flashdata('msg', '<div class="alert alert-danger">Pesanan " '.$data->kode_pesanan.' " telah gagal di proses</div>');
				}
				redirect('admin/orders');
			}
		}
	}

function rejected($id=null){

			$this->db->where('id',decode_id($id));
			$data=$this->db->get('orders_undangan')->row();

			$arrs = array('confirm_from'=>$_SESSION['users'],'pesan'=>$_POST['pesan'],'status'=>$_POST['status']);

			$this->db->where('id',decode_id($id));
			$this->db->set('confirm_at','NOW()',false);
			$this->db->update('orders_undangan',$arrs);
			$this->session->set_flashdata('msg', '<div class="alert alert-warning">Pesanan " '.$data->kode_pesanan.' " telah dibatalkan</div>');
			
			redirect('admin/orders');


}



}