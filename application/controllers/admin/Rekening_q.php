<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekening_q extends CI_Controller {
function __construct()
    {
        parent::__construct();
            $this->load->library('image_lib');
            $this->load->model('Rekening_q_model');
            $this->load->model('Image_model');
            $this->load->library('form_validation');
        if ((empty($this->session->userdata('users')))&&(empty($this->session->userdata('users')))) {
            redirect('admin/login');
        }
         
    }
	 

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'rekening_q/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'rekening_q/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'rekening_q/index.html';
            $config['first_url'] = base_url() . 'rekening_q/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Rekening_q_model->total_rows($q);
        $rekening_q = $this->Rekening_q_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'rekening_q_data' => $rekening_q,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
         

                
       
        $data['page']='rekening_q_list';
        $this->load->view('dashboard/template',$data);
    }

   

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/rekening_q/create_action'),
        'id' => set_value('id'),
        'nama_bank' => set_value('nama_bank'),
         'atas_nama' => set_value('atas_nama'),
        'photo' => set_value('photo'),
        'kode_rekening' => set_value('kode_rekening'),
        'status' => set_value('status'),
    );
         

             
                
       
        $data['page']='rekening_q_form';
        $this->load->view('dashboard/template',$data);
    }
    
    public function create_action() 
    {
        $this->_rules();
        $is_photo="";
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            for ($i=0; $i <  count($_POST['photo']) ; $i++) {
                      if (!empty($_POST['photo'][$i])) {
                             $this->Image_model->delete(trim($_POST['photo'][$i]));
                             $is_photo= trim($_POST['photo'][$i]);                   
                      }
                      
                    }
            // var_dump($_POST);die();
            $data = array(
            'atas_nama' => $this->input->post('atas_nama',TRUE),
            'nama_bank' => $this->input->post('nama_bank',TRUE),
            'photo' => $is_photo,
            'kode_rekening' => $this->input->post('kode_rekening',TRUE),
            'status' => $this->input->post('status',TRUE),
            );

            $this->Rekening_q_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success">Create Record Success</div>');
            redirect(site_url('admin/rekening_q'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Rekening_q_model->get_by_id(decode_id($id));

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/rekening_q/update_action'),
        'id' => set_value('id', $row->id),
        'atas_nama' => set_value('atas_nama', $row->atas_nama),
        'nama_bank' => set_value('nama_bank', $row->nama_bank),
        'photo' => set_value('photo'),
        'kode_rekening' => set_value('kode_rekening', $row->kode_rekening),
        'status' => set_value('status', $row->status),
        );
             

        $data['page']='rekening_q_form';
        $this->load->view('dashboard/template',$data);
                } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Record Not Found</div>');
            redirect(site_url('admin/rekening_q'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();
        $is_photo="";
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
             if((isset($_POST['photo']))&&(!empty($_POST['photo']))){
                    for ($i=0; $i <  count($_POST['photo']) ; $i++) {
                          if (!empty($_POST['photo'][$i])) {
                                $this->Rekening_q_model->is_unlink($this->input->post('id', TRUE));
                                $this->Image_model->delete(trim($_POST['photo'][$i]));
                                $is_photo= trim($_POST['photo'][$i]);                   
                          }                      
                    }
            }
         if(!empty($is_photo)){
            $data = array(
                'atas_nama' => $this->input->post('atas_nama',TRUE),
                'nama_bank' => $this->input->post('nama_bank',TRUE),
                'kode_rekening' => $this->input->post('kode_rekening',TRUE),
                'photo' =>$is_photo,
                'status' => $this->input->post('status',TRUE),
                );
        } else{
            $data = array(
                'atas_nama' => $this->input->post('atas_nama',TRUE),
                'nama_bank' => $this->input->post('nama_bank',TRUE),
                'kode_rekening' => $this->input->post('kode_rekening',TRUE),
                'status' => $this->input->post('status',TRUE),
                );
        }
            $this->Rekening_q_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success">Update Record Success</div>');
            redirect(site_url('admin/rekening_q'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Rekening_q_model->get_by_id(decode_id($id));

        if ($row) {
            $this->Rekening_q_model->delete(decode_id($id));
            $this->session->set_flashdata('message', '<div class="alert alert-success">Delete Record Success</div>');
            redirect(site_url('admin/rekening_q'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Record Not Found</div>');
            redirect(site_url('admin/rekening_q'));
        }
    }

        public function ajax_upload_photo()
          {
            $user_id = $this->input->post('user_id');
            $from_photo=$this->input->post('from_photo');

            // initialize FileUploader
            $FileUploader = new FileUploader('file', array(
                'limit' => 1,
                'maxSize' => null,
                'fileMaxSize' => 8,
                'extensions' => ['PNG','JPG','JPEG','jpg', 'jpeg', 'png', 'gif'],
                'required' => false,
                'uploadDir' => FCPATH.'assets/images/rekening/',
                'title' => 'photo_{random}_{timestamp}',
                'replace' => false,
                'listInput' => false,
                'files' => null
            ));
          
            // call to upload the files
            $data = $FileUploader->upload();
            $arr = array('photo'=>$data['files'][0]['name']);
            $this->Image_model->add($arr);
           

            // export to js
            echo json_encode($data);
            exit;
          }

          public function ajax_remove_photo()
              {
                $filename = $_POST['file'];
                $review_id = $_POST['review_id'];
                if (isset($filename)) {
                    $file = FCPATH.'assets/images/rekening/'.$filename;
                    if(file_exists($file))
                    unlink($file);
                }
              }



        function delete_image($id){
          $arr=array('photo'=>'');
                          
          $res=$this->Rekening_q_model->update_photo(decode_id($id),$arr);
            if ($res) {
               $this->session->set_flashdata('message', '<div class="alert alert-success">Delete Image Success</div>');
          
              }
             else{
                   $this->session->set_flashdata('message', '<div class="alert alert-danger">Delete Image Unsuccess</div>');
           
              }
           redirect($_SERVER['HTTP_REFERER']);
    
        }

        function ubah_photo(){
         
          $id= $this->input->post('id');
          $photo_old= trim($this->input->post('photo_old'));
          $photo_review =trim($_FILES['photos_review']['name']);
          
          if ($photo_old!=$photo_review) {
             $file = FCPATH.'assets/images/rekening/'.$photo_old;// hapus file di server
                            if(file_exists($file)){
                            unlink($file);
                          }
                          $nmfile = "file_".time();
                          $config['upload_path'] = './assets/images/rekening/';
                          $config['allowed_types'] = 'JPEG|JPG|jpg|jpeg|png|PNG|gif|GIF'; 
                          $config['file_name'] = $nmfile;
                          $this->load->library('upload',$config);

                         if(!$this->upload->do_upload('photos_review')){
                           $this->session->set_flashdata('error',$this->upload->display_errors());
                         }
                         else{
                           $gbr= $this->upload->data(); 
                          $arr=array('photo'=>trim($gbr['file_name']));
                          $this->Image_model->delete(trim($gbr['file_name']));
                          $res=$this->Rekening_q_model->update_photo($id,$arr);
                           if ($res) {
                                         $this->session->set_flashdata('message', '<div class="alert alert-success">Upload Image Success</div>');
           
                                        }
                                        else{
                                           $this->session->set_flashdata('message', '<div class="alert alert-danger">Upload Image Unsuccess</div>');
           
                                        }
                         }
          }
          else{
                          $nmfile = "file_".time();
                          $config['upload_path'] = './assets/images/rekening/';
                          $config['allowed_types'] = 'JPEG|JPG|jpg|jpeg|png|PNG'; 
                          $config['file_name'] = $nmfile;
                          $this->load->library('upload',$config);

                         if(!$this->upload->do_upload('photos_review')){
                          $this->session->set_flashdata('error',$this->upload->display_errors());
                         }
                         else{
                           $gbr= $this->upload->data(); 
                          $arr=array('photo'=>trim($gbr['file_name']));
                          $this->Image_model->delete(trim($gbr['file_name']));
                          $res=$this->Rekening_q_model->update_photo($id,$arr);
                           if ($res) {
                                         $this->session->set_flashdata('message', '<div class="alert alert-success">Upload Image Success</div>');
           
                                        }
                                        else{
                                           $this->session->set_flashdata('message', '<div class="alert alert-danger">Upload Image Unsuccess</div>');
           
                                        }
                         }

          }
         redirect($_SERVER['HTTP_REFERER']);
        }



        public function _rules() 
            {
          $this->form_validation->set_rules('atas_nama', 'atas nama', 'trim|required');
          // $this->form_validation->set_rules('photo', 'photo', 'trim|required');
          $this->form_validation->set_rules('kode_rekening', 'kode rekening', 'trim|required');
          $this->form_validation->set_rules('status', 'status', 'trim|required');

          $this->form_validation->set_rules('id', 'id', 'trim');
          $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
            }

//-------------	 
}