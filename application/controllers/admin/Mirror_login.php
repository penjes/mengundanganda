<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mirror_login extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');        
	    $this->fpath  = "admin/";
	    $this->load->helper('common_helper');
    
    }
	 

    function index(){
    	$this->listing(0);
    }



	public function listing($page=0)
	{
		
		$limit =25;

		if((isset($_GET['search']))&&(empty($_GET['search']))){
			$this->db->like('mempelai_pria',$_GET['search']);
			$this->db->or_like('mempelai_wanita',$_GET['search']);
			$this->db->or_like('kode_internal',$_GET['search']);
		}
		$this->db->limit($limit,$page);
		$this->db->where('status','1');
		$data['konfiguration'] = $this->db->get('konfiguration')->result();

		$total_row = $this->db->get("konfiguration")->num_rows();
	    
	    $config["base_url"] = base_url()."admin/mirror_login/listing";
	    $config["uri_segment"] = 3;
	    if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
	    $config['full_tag_open'] = '<ul class="pagination" id="pagenumber-list-ajax">';
	    $config['full_tag_close'] = '</ul>';
	    $config['first_link'] = '';
	    $config['first_tag_open'] = '';
	    $config['first_tag_close'] = '';
	    $config['last_link'] = '';
	    $config['last_tag_open'] = '';
	    $config['last_tag_close'] = '';
	    $config['next_link'] = 'Next &raquo;';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['prev_link'] = '&laquo; Prev';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    // $config['num_links'] = $total_row;
	    // $config['use_page_numbers'] = FALSE;
	    $config["total_rows"] = $total_row;
	    $data['no']=$page;
	    $this->pagination->initialize($config);
	    $data["pagination"] = $this->pagination->create_links();
	    $data['page']='mirror_login';
		$this->load->view('dashboard/template',$data);
	}




	function mirror_login_user($kode_internal=null){
		$this->db->where('kode_internal',$kode_internal);
		$biodata=$this->db->get('internal_user')->row();
		if(!empty($kode_internal)){
			$_SESSION['kode_mirror_internal'] = $kode_internal;	
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Anda berhasil login sebagai '..'</div>');
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Pengisian Data Gagal !</div>');
		}
		redirect('admin/home');
		
	}


   function unset_mirror_login(){
   		if(!empty($_SESSION['kode_mirror_internal'])){
			$_SESSION['kode_mirror_internal'] = '';	
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Mirror login sudah tidak aktif</div>');
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Proses Gagal !</div>');
		}
		redirect('admin/mirror_login');
   }



}