<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {
	function __construct()
    {
        parent::__construct();
           $this->load->library('image_lib');

        if ((empty($this->session->userdata('users')))&&(empty($this->session->userdata('users')))) {
            redirect('admin/login');
        }
         
    }
	 
	 
	public function index()
	{
		$data['page']='events';
						 $this->db->order_by('id','asc');
		$data['event'] = $this->db->get('wedding_event')->result();
		$this->load->view('dashboard/template',$data);
	}
	 

	public function add(){
		$acara=$this->db->get('wedding_event')->num_rows();
		
		$data = array(	'ids' =>'',
						'title' => '',
						'description' => '',
						'start_1' => '',
						'start_2' => '',
						'end_1' => '',
						'end_2' => '',
						'event_date' =>''
					);
		if(empty($acara)){
			$data['acara'] = 1;
		}else{
			$data['acara'] = $acara + 1;
		}
		$data['page']='events_add';
		$data['action'] = 'action_add';
		$this->load->view('dashboard/template',$data);
	}

	function action_add(){
		$date_event = explode('/',$_POST['event_date']);
		$tgl_event = $date_event[2].'-'.$date_event[0].'-'.$date_event[1];
		
		if($_SESSION['user_level']==1){
			$kode_internal = $_SESSION['kode_mirror_internal'];
		}else{
			$kode_internal = $_SESSION['kode_internal'];
		}
		$arr = array(
					'start_at' =>$_POST['start_1'].':'.$_POST['start_2'],
					'end_at' =>$_POST['end_1'].':'.$_POST['end_2'],
					'title' =>$_POST['title'],
					'description'=>$_POST['description'],
					'event_date' =>$tgl_event,
					'kode_internal' => $kode_internal
					);
		$this->db->set('last_update','NOW()',false);
		$log=$this->db->insert('wedding_event',$arr);
		// var_dump($this->db->last_query());die();
		if($log){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil isi Data</div>');
			
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Pengisian Data Gagal !</div>');
		}
		redirect('admin/events');
	}


	function edit($id,$no){
		// $id = $_POST['ids'];
		$this->db->where('id',decode_id($id));
		$event = $this->db->get('wedding_event')->row();
		$date_event = explode('-',$event->event_date);
		$tgl_event = $date_event[1].'/'.$date_event[2].'/'.$date_event[0];
		
		$data = array(	'ids' => encode_id($event->id),
						'title' => $event->title,
						'description' => $event->description,
						'start_1' => substr($event->start_at,0,2),
						'start_2' => substr($event->start_at,3,2),
						'end_1' => substr($event->end_at,0,2),
						'end_2' => substr($event->end_at,3,2),
						'event_date' =>$tgl_event
					);
		$data['acara'] =$no;
		$data['page']='events_add';
		$data['action'] = 'action_edit';
		$this->load->view('dashboard/template',$data);

	}
	function action_edit(){
		$date_event = explode('/',$_POST['event_date']);
		$tgl_event = $date_event[2].'-'.$date_event[0].'-'.$date_event[1];
		
		if($_SESSION['user_level']==1){
			$kode_internal = $_SESSION['kode_mirror_internal'];
		}else{
			$kode_internal = $_SESSION['kode_internal'];
		}

		$arr = array(
					'start_at' =>$_POST['start_1'].':'.$_POST['start_2'],
					'end_at' =>$_POST['end_1'].':'.$_POST['end_2'],
					'title' =>$_POST['title'],
					'description'=>$_POST['description'],
					'event_date' =>$tgl_event,
					'kode_internal' => $kode_internal,
					'user_action' => $_SESSION['users']
					);

		$this->db->where('id',decode_id($_POST['ids']));
		$this->db->set('last_update','NOW()',false);
		$log=$this->db->update('wedding_event',$arr);
		if($log){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil Rubah Data</div>');
			
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Edit Data Gagal !</div>');
		}
		redirect('admin/events');
	}

	public function delete($id){
		if($_SESSION['user_level']==1){
			$this->db->where('kode_internal',$_SESSION['kode_mirror_internal']);
		}else{
			$this->db->where('kode_internal',$_SESSION['kode_internal']);
		}
		$this->db->where('id',decode_id($id));
		$log=$this->db->delete('wedding_event');
		// var_dump($this->db->last_query());die();
		if($log){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil Hapus Data</div>');
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Hapus Data Gagal !</div>');
		}
		redirect('admin/events');
	}

}
