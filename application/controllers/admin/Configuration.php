<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuration extends CI_Controller {
function __construct()
    {
        parent::__construct();
           $this->load->library('image_lib');
           
        if ((empty($this->session->userdata('users')))&&(empty($this->session->userdata('users')))) {
            redirect('admin/login');
        }
         
    }
	 
	 
	public function index()
	{
		$data['page']='configuration';
						 // $this->db->order_by('id','asc');
		$_SESSION['kode_mirror_internal']='admin';//for tes
		if($_SESSION['user_level']==1){
			$kode_internal = $_SESSION['kode_mirror_internal'];
		}else{
			$kode_internal = $_SESSION['kode_internal'];
		}


		$this->db->select("`id`,
						  					 `bg`, 
						  					 paket,
						  					 is_template,
						  					 `mempelai_pria`, 
						  					 `photo_pria`,
						  					  `mempelai_wanita`, 
						  					  `photo_wanita`, 
						  					  `ayah_pria`, 
						  					  `ibu_pria`, 
						  					  `ayah_wanita`, 
						  					  `ibu_wanita`,
						  					   `content_pria`,
						  					   `content_wanita`,
						  					   `acara_resepsi`,
						  					   date_format(`acara_resepsi`,'%M  %d,  %Y')as acara,
						  					   `start_at`,
						  					   `end_at`,
						  					   `email`,
						  					   `telp`,
						  					   `whatsapp`,
						  					    `alamat`,
						  					    `kecamatan`,
						  					    `kota`,
						  					    `latitude`,
						  					    `longtitude`,
						  					    `kode_internal`,
						  					    `status`,
						  					    `expired_date`,
						  					    `author_is`, `created_at`");
		$this->db->where('kode_internal',$kode_internal);
		$this->db->where('status','1');
		$data['biodata'] = $this->db->get('konfiguration')->row();
		// var_dump($this->db->last_query());die();

		$this->db->where('kode_internal',$kode_internal);
		$data['slider'] = $this->db->get('slider')->result();

		$this->db->where('kode_internal',$kode_internal);
		$data['tot_slider'] = $this->db->get('slider')->num_rows();
		// hanya admin yang boleh menambahkan konfigurasi
		// if (empty($data['biodata'])) {
		// 	$data['action']='add';
		// }else{
			$data['action']='edit';
		// }
		$this->load->view('dashboard/template',$data);
	}
	 

	public function add(){
		 
		$data = array(	'ids' =>'',
						'email' => '',
						'telp' => '',
						'whatsapp' => '',
						'alamat' => '',
						'mempelai_pria' => '',
						'mempelai_wanita' => '',
						'ayah_pria' => '',
						'ibu_pria' => '',
						'ayah_wanita' => '',
						'ibu_wanita' => '',
						'acara_resepsi' =>'',
						'start_1' => '',
						'start_2' => '',
						'end_1' => '',
						'end_2' => ''
						
					);
		 
		$data['page']='configuration_add';
		$data['action'] = 'action_add';
		$this->load->view('dashboard/template',$data);
	}

	function action_add(){
 		$date_event = explode('/',$_POST['acara_resepsi']);
		$tgl_event = $date_event[2].'-'.$date_event[0].'-'.$date_event[1];
		
		$arr = array(	'email' => $_POST['email'],
						'telp' => $_POST['telp'],
						'whatsapp' => $_POST['whatsapp'],
						'alamat' => $_POST['alamat'],
						'mempelai_pria' => $_POST['mempelai_pria'],
						'mempelai_wanita' => $_POST['mempelai_wanita'],
						'ayah_pria' => $_POST['ayah_pria'],
						'ibu_pria' => $_POST['ibu_pria'],
						'ayah_wanita' => $_POST['ayah_wanita'],
						'ibu_wanita' => $_POST['ibu_wanita'],
						'acara_resepsi' =>$tgl_event,
						'start_at' =>$_POST['start_1'].':'.$_POST['start_2'],
						'end_at' =>$_POST['end_1'].':'.$_POST['end_2'],
					);
		$log=$this->db->insert('konfiguration',$arr);
		// var_dump($this->db->last_query());die();
		if($log){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil isi Data</div>');
			
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Pengisian Data Gagal !</div>');
		}
		redirect('admin/configuration');
	}


	function edit($id){
		// $id = $_POST['ids'];
		$this->db->where('id',decode_id($id));
		$event = $this->db->get('konfiguration')->row();
		$date_event = explode('-',$event->acara_resepsi);
		$tgl_event = $date_event[1].'/'.$date_event[2].'/'.$date_event[0];
		
		$data = array(	'ids' => encode_id($event->id),
						'email' => $event->email,
						'telp' => $event->telp,
						'whatsapp' => $event->whatsapp,
						'alamat' => $event->alamat,
						'mempelai_pria' => $event->mempelai_pria,
						'mempelai_wanita' => $event->mempelai_wanita,
						'ayah_pria' => $event->ayah_pria,
						'ibu_pria' => $event->ibu_pria,
						'ayah_wanita' => $event->ayah_wanita,
						'ibu_wanita' => $event->ibu_wanita,
						'start_1' => substr($event->start_at,0,2),
						'start_2' => substr($event->start_at,3,2),
						'end_1' => substr($event->end_at,0,2),
						'end_2' => substr($event->end_at,3,2),
						'acara_resepsi' =>$tgl_event
					); 
		$data['page']='configuration_add';
		$data['action'] = 'action_edit';
		$this->load->view('dashboard/template',$data);

	}
	function action_edit(){
		$date_event = explode('/',$_POST['acara_resepsi']);
		$tgl_event = $date_event[2].'-'.$date_event[0].'-'.$date_event[1];
		// var_dump($_POST);die();

		$arr = array(	'email' => $_POST['email'],
						'telp' => $_POST['telp'],
						'whatsapp' => $_POST['whatsapp'],
						'alamat' => $_POST['alamat'],
						'mempelai_pria' => $_POST['mempelai_pria'],
						'mempelai_wanita' => $_POST['mempelai_wanita'],
						'ayah_pria' => $_POST['ayah_pria'],
						'ibu_pria' => $_POST['ibu_pria'],
						'ayah_wanita' => $_POST['ayah_wanita'],
						'ibu_wanita' => $_POST['ibu_wanita'],
						'acara_resepsi' =>$tgl_event,
						'start_at' =>$_POST['start_1'].':'.$_POST['start_2'],
						'end_at' =>$_POST['end_1'].':'.$_POST['end_2'],
					);

		if($_SESSION['user_level']==1){
			$kode_internal = $_SESSION['kode_mirror_internal'];
		}else{
			$kode_internal = $_SESSION['kode_internal'];
		}

		$this->db->where('kode_internal',$kode_internal);
		$this->db->where('id',decode_id($_POST['ids']));
		$log=$this->db->update('konfiguration',$arr);
		if($log){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil Rubah Data</div>');
			
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Edit Data Gagal !</div>');
		}
		redirect('admin/configuration');
	}

//delete konfirgurasi hanya bisa dilakukan oleh super admin
	public function delete($id){
		if($_SESSION ==1){
			$this->db->where('id',decode_id($id));
			$log=$this->db->delete('configuration');
		}else{
			$log=false;
		}
		// var_dump($this->db->last_query());die();
		if($log){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil Hapus Data</div>');
			
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Hapus Data Gagal !</div>');
		}
		redirect('admin/configuration');
	}



public function ajax_upload_photo()
  {
     
    // initialize FileUploader
    $FileUploader = new FileUploader('file', array(
        'limit' => 8,
        'maxSize' => null,
        'fileMaxSize' => 8,
        'extensions' => ['PNG','JPG','JPEG','jpg', 'jpeg', 'png', 'gif'],
        'required' => false,
        'uploadDir' => FCPATH.'assets/images/manten/',
        'title' => 'photo_{random}_{timestamp}',
        'replace' => false,
        'listInput' => false,
        'files' => null
    ));
  
    // call to upload the files
    $data = $FileUploader->upload();
    $arr = array('photo'=>$data['files'][0]['name']);
    $this->db->set('created_at','NOW()',false);
    $this->db->insert('image_temp',$arr);
    echo json_encode($data);
    exit;
  }
public function ajax_remove_photo()
              {
                $filename = $_POST['file'];
                echo $filename;
                if (isset($filename)) {
                    $file = FCPATH.'assets/images/manten/'.$filename;
                    if(file_exists($file))
                    unlink($file);
                }
              }

function ubah_photo(){

  	 if ((sizeof($_POST) > 0)) {
  	 	$photo = NULL;
            $arr = NULL;
              foreach($_POST as $key => $val)
              {
                if(!is_array($val))
                { 
                  if ($key !='fileuploader-list-file') {
                if ($key !='id') {
                     $arr[$key] = trim($val); 
               }
                  }
                 
                }
              }
  	 
        	if(!empty($_POST['photo'])){
        		for ($i=0; $i <  count($_POST['photo']) ; $i++) {
                      if (!empty($_POST['photo'][$i])) {
	                    $this->db->where('photo',trim($_POST['photo'][$i]));
	                    $this->db->delete('image_temp');
	                    $photo = trim($_POST['photo'][$i]); 
                      }
                      
              }
        	}
              
            $photo_old = $_POST['photo_old'];
            if(!empty($photo)){
	            if(!empty($photo_old)){		
	            		if($photo !== $photo_old){
	            	
									$file = FCPATH.'assets/images/manten/'.$photo_old;// hapus file di server             
			                          if(file_exists($file)){
			                            unlink($file);
			                          }
			                          //------ compress -------------

			                          $config['image_library'] = 'gd2';
			                          $config['source_image'] = '_assets/images/galleri/'.$photo;
			                          $config['maintain_ratio'] = TRUE;
			                          $config['width']     = 900;
			                          $config['height']   = 600;
			                          $this->image_lib->initialize($config);
			                          $this->image_lib->resize();
			                          //-----------------------------
						}else{
							$photo = $photo_old;
						}
	            }
	        }else{
							$photo = $photo_old;
						}


            if(!empty($_POST['content_pria_sts'])){
            $photo_desc = 'photo_pria';
            $content = 'content_pria';
            $isi_content = $_POST['content'];
            
            }

            if(!empty($_POST['content_wanita_sts'])){
            $photo_desc = 'photo_wanita';
            $content = 'content_wanita';
            $isi_content = $_POST['content'];
            
            }
 
            $arr =array($photo_desc=>trim($photo),$content=>$isi_content);
                      
            $this->db->where('id',decode_id($_POST['ids'])); 
            $log=$this->db->update('konfiguration',$arr);
                      

           if ($log) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success">Product berhasil di tambahkan</div>');
           }else{
            $this->session->set_flashdata('msg', '<div class="alert alert-danger">Produk gagal ditambahkan</div>');
           }
           redirect('admin/configuration');
           exit();
        }
         $this->session->set_flashdata('msg', '<div class="alert alert-danger">Foto gagal ditambahkan</div>');
         redirect('admin/configuration');


	}

function edit_photo($id,$sts){
		// $id = $_POST['ids'];
		$this->db->where('id',decode_id($id));
		$event = $this->db->get('konfiguration')->row();
		 
		if(decode_id($sts)=='pria'){

			$data = array(	'ids' => encode_id($event->id),
							'photo_old' => $event->photo_pria,
							'content' => $event->content_pria,
							'content_pria_sts' =>'1',
							'content_wanita_sts' =>'0'
						); 

		}else{
					$data = array(	'ids' => encode_id($event->id),
						'photo_old' => $event->photo_wanita,
						'content' => $event->content_wanita,
						'content_wanita_sts' =>'1',
						'content_pria_sts' =>'0'
					); 

		}
		$data['page']='configuration_upload';
		$data['action'] = 'ubah_photo';
		$this->load->view('dashboard/template',$data);

	}


}
