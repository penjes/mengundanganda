<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gateway_internal extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');        
	    $this->fpath  = "admin/";
	    $this->load->helper('common_helper');
    
    }
	 
	public function index()
	{
		$data['page']='gateway_internal';
		$this->load->view('dashboard/content/gateway_internal',$data);
	}

	function proses($user=""){
        $data['title'] ='Login Gateway';
         if (sizeof($_POST) >0) {

            // var_dump($_POST);
           
				$users = $this->input->post('username');
            	$pass = trim($this->input->post('password'));
            	$pass = encode_id($pass);
            	

            	$this->db->where('username',$users);
		    	$this->db->where('password',$pass);
		    	$this->db->from('internal_user');		    	
            	$log=$this->db->get()->row();
            	if (empty($log)) {
            		$this->session->set_flashdata('message', '<div class="alert alert-danger">Email atau Password salah</div>');
                    //jika user tidak ditemukan   
                    redirect('admin/gateway_internal');
            	}else{
            		if (empty($log->status)) {
            			$this->session->set_flashdata('message', '<div class="alert alert-danger">Akun belum akif, silahkan konfirmasi link yang kami kirim ke email anda untuk mengaktifkan akun</div>');
            		  //jika akun belum melakukan konfirmasi akun email 
                        redirect('admin/gateway_internal');
                    }
                    else{
                        //jika user ditemukan
						$this->db->where('id',$log->id); 
    					$this->db->set('last_login','NOW()',false);
    					$this->db->update('internal_user');

                        $this->session->set_userdata('user_level',$log->level_user);
                        $this->session->set_userdata('user_ids',$log->id);
                        $this->session->set_userdata('users',$log->username); 
                        $this->session->set_userdata('kode_internal',encode_id($log->kode_internal));                   
                        redirect('admin/home');
                     exit();
                    }
        	   }
        }
        $data['config'] = $this->db->get('configuration')->row_array();
        $this->load->view($this->fpath.'gateway_internal',$data);

    }
	
}
