<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Our_story extends CI_Controller {
	function __construct()
    {
        parent::__construct();
           $this->load->library('image_lib');

        if ((empty($this->session->userdata('users')))&&(empty($this->session->userdata('users')))) {
            redirect('admin/login');
        }
         
    }
	 
	 
	public function index()
	{
		$data['page']='our_story';
						 $this->db->order_by('id','asc');
		$data['our_story'] = $this->db->get('our_story')->result();
		$this->load->view('dashboard/template',$data);
	}
	 

	public function add(){
		$acara=$this->db->get('our_story')->num_rows();
		
		$data = array(	'ids' =>'',
						'title' => '',
						'content' => '',
						'history_date' => '',
						'tampilkan_history_date' => '',
						'urutan' => '' ,
						'photo_old' =>''
					);
		if(empty($acara)){
			$data['acara'] = 1;
		}else{
			$data['acara'] = $acara + 1;
		}
		$data['page']='our_story_add';
		$data['action'] = 'action_add';
		$this->load->view('dashboard/template',$data);
	}

	function action_add(){
		$tampilkan_history_date=0;
		if(isset($_POST['tampilkan_history_date'])){
			if($_POST['tampilkan_history_date']=='on'){				
				echo 'wooi';	
				$tampilkan_history_date=1;
			}else{
				$tampilkan_history_date=0;
			}
		}
		// var_dump($_POST);die();
		$date_event = explode('/',$_POST['history_date']);
		$tgl_event = $date_event[2].'-'.$date_event[0].'-'.$date_event[1];
		
					for ($i=0; $i <  count($_POST['photo']) ; $i++) {
                      if (!empty($_POST['photo'][$i])) {
                      	//------ compress -------------

		                          $config['image_library'] = 'gd2';
		                          $config['source_image'] = '_assets/images/story/'.trim($_POST['photo'][$i]);
		                          $config['maintain_ratio'] = TRUE;
		                          $config['width']     = 900;
		                          $config['height']   = 600;
		                          $this->image_lib->initialize($config);
		                          $this->image_lib->resize();
		                          //-----------------------------
                      $this->db->where('photo',trim($_POST['photo'][$i]));
                      $this->db->delete('image_temp');
                      $arr = array( 
									'title' =>$_POST['title'],
									'content'=>$_POST['content'],
									'history_date' => $tgl_event,
									'tampilkan_date_history' => $tampilkan_history_date,
									'image'=>trim($_POST['photo'][$i]),
									'urutan' => $_POST['urutan']
									);
                       
                      $log=$this->db->insert('our_story',$arr);
                      }
                      
                    }		

		if($log){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil isi Data</div>');
			
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Pengisian Data Gagal !</div>');
		}
		redirect('admin/our_story');
	}


	function edit($id,$no){
		// $id = $_POST['ids'];
		$this->db->where('id',decode_id($id));
		$page = $this->db->get('our_story')->row();
		$date_event = explode('-',$page->history_date);
		$tgl_event = $date_event[1].'/'.$date_event[2].'/'.$date_event[0];
		
		$data = array(	'ids' =>$page->id,
						'title' => $page->title,
						'content' => $page->content,
						'history_date' => $tgl_event,
						'tampilkan_history_date' => $page->tampilkan_date_history,
						'urutan' => $page->urutan,
						'photo_old' => $page->image
					);
		$data['page']='our_story_add';
		$data['action'] = 'action_edit';
		$this->load->view('dashboard/template',$data);

	}
	function action_edit(){
		$photo='';
		$tampilkan_history_date=0;
		$date_event = explode('/',$_POST['history_date']);
		$tgl_event = $date_event[2].'-'.$date_event[0].'-'.$date_event[1];
		if(isset($_POST['tampilkan_history_date'])){
			if($_POST['tampilkan_history_date']=='on'){				
				echo 'wooi';	
				$tampilkan_history_date=1;
			}else{
				$tampilkan_history_date=0;
			}
		}
		if(!empty($_POST['photo'])){
        		for ($i=0; $i <  count($_POST['photo']) ; $i++) {
                      if (!empty($_POST['photo'][$i])) {
	                    $this->db->where('photo',trim($_POST['photo'][$i]));
	                    $this->db->delete('image_temp');
	                    $photo = trim($_POST['photo'][$i]); 
                      }
                      
              }
        	}
              
            $photo_old = $_POST['photo_old'];
              if(!empty($photo)){     
                if(!empty($photo_old)){		
                		if($photo !== $photo_old){
                	  								$file = FCPATH.'assets/images/story/'.$photo_old;// hapus file di server             
          		                          if(file_exists($file)){
          		                            unlink($file);
          		                          }
          		                          //------ compress -------------

          		                          $config['image_library'] = 'gd2';
          		                          $config['source_image'] = '_assets/images/story/'.$photo;
          		                          $config['maintain_ratio'] = TRUE;
          		                          $config['width']     = 900;
          		                          $config['height']   = 600;
          		                          $this->image_lib->initialize($config);
          		                          $this->image_lib->resize();
          		                          //-----------------------------
          					}else{
          						$photo = $photo_old;
          					}
                }
              }else{
                    $photo = $photo_old;
              }
		$arr = array(
					'title' =>$_POST['title'],
					'content'=>$_POST['content'],
					'history_date' => $tgl_event,
					'tampilkan_date_history' => $tampilkan_history_date,
					'urutan' => $_POST['urutan'] ,
					'image' =>$photo
					);
		$this->db->where('id',decode_id($_POST['ids']));
		$log=$this->db->update('our_story',$arr);
		if($log){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil Rubah Data</div>');
			
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Edit Data Gagal !</div>');
		}
		redirect('admin/our_story');
	}

	public function delete($id){
		$this->db->where('id',decode_id($id));
		$key=$this->db->get('our_story')->row();

		$file = FCPATH.'assets/images/story/'.$key->image;
		if(file_exists($file)){
                            unlink($file);
        }

		$this->db->where('id',decode_id($id));
		$log=$this->db->delete('our_story');
		// var_dump($this->db->last_query());die();
		if($log){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil Hapus Data</div>');
			
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Hapus Data Gagal !</div>');
		}
		redirect('admin/our_story');
	}


public function ajax_upload_photo()
  {
     
    // initialize FileUploader
    $FileUploader = new FileUploader('file', array(
        'limit' => 1,
        'maxSize' => null,
        'fileMaxSize' => 8,
        'extensions' => ['PNG','JPG','JPEG','jpg', 'jpeg', 'png', 'gif'],
        'required' => false,
        'uploadDir' => FCPATH.'assets/images/story/',
        'title' => 'photo_{random}_{timestamp}',
        'replace' => false,
        'listInput' => false,
        'files' => null
    ));
  
    // call to upload the files
    $data = $FileUploader->upload();
    $arr = array('photo'=>$data['files'][0]['name']);
    $this->db->set('created_at','NOW()',false);
    $this->db->insert('image_temp',$arr);
    echo json_encode($data);
    exit;
  }
public function ajax_remove_photo()
              {
                $filename = $_POST['file'];
                echo $filename;
                if (isset($filename)) {
                    $file = FCPATH.'assets/images/story/'.$filename;
                    if(file_exists($file))
                    unlink($file);
                }
              }





}
