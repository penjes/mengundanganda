<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Encode_id extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');        
	    $this->fpath  = "admin/";
	    $this->load->helper('common_helper');
    
    }


    function index($id=null){
    	$this->encode_ids($id);
    }

    function encode_ids($id=null){
        echo encode_id($id);
    }


    function decode_ids($id=null){
        echo decode_id($id);
    }

}