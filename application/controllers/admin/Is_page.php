<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Is_page extends CI_Controller {
	function __construct()
    {
        parent::__construct();
           $this->load->library('image_lib');

        if ((empty($this->session->userdata('users')))&&(empty($this->session->userdata('users')))) {
            redirect('admin/login');
        }
         
    }
	 
	 
	public function index()
	{
		$data['page']='is_page';
						 $this->db->order_by('id','asc');
		$data['is_page'] = $this->db->get('is_page')->result();
		$this->load->view('dashboard/template',$data);
	}
	 

	public function add(){
		$acara=$this->db->get('is_page')->num_rows();
		
		$data = array(	'ids' =>'',
						'title' => '',
						'content' => '',
						'head_title' => '',
						'page_name' => '' 
					);
		if(empty($acara)){
			$data['acara'] = 1;
		}else{
			$data['acara'] = $acara + 1;
		}
		$data['page']='is_page_add';
		$data['action'] = 'action_add';
		$this->load->view('dashboard/template',$data);
	}

	function action_add(){
		 
		$arr = array(
					'page_name' =>$_POST['page_name'],
					'head_title' =>$_POST['head_title'],
					'title' =>$_POST['title'],
					'content'=>$_POST['content'] 
					);
		$log=$this->db->insert('is_page',$arr);
		// var_dump($this->db->last_query());die();
		if($log){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil isi Data</div>');
			
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Pengisian Data Gagal !</div>');
		}
		redirect('admin/is_page');
	}


	function edit($id,$no){
		// $id = $_POST['ids'];
		$this->db->where('id',decode_id($id));
		$page = $this->db->get('is_page')->row();
		 
		$data = array(	'ids' =>$page->id,
						'title' => $page->title,
						'content' => $page->content,
						'head_title' => $page->head_title,
						'page_name' => $page->page_name,
						'photo_old' => $page->image 
					);
		$data['page']='is_page_add';
		$data['action'] = 'action_edit';
		$data['acara'] =$page->page_name;
		$this->load->view('dashboard/template',$data);

	}
	function action_edit(){
		$photo = NULL;
            $arr = NULL;
            
  	 
        	if(!empty($_POST['photo'])){
        		for ($i=0; $i <  count($_POST['photo']) ; $i++) {
                      if (!empty($_POST['photo'][$i])) {
	                    $this->db->where('photo',trim($_POST['photo'][$i]));
	                    $this->db->delete('image_temp');
	                    $photo = trim($_POST['photo'][$i]); 
                      }
                      
              }
        	}
              
            $photo_old = $_POST['photo_old'];
            if(!empty($photo)){
            	if(!empty($photo_old)){		
            		if($photo !== $photo_old){
            	
								$file = FCPATH.'assets/images/is_page/'.$photo_old;// hapus file di server             
		                          if(file_exists($file)){
		                            unlink($file);
		                          }
		                          //------ compress -------------

		                          $config['image_library'] = 'gd2';
		                          $config['source_image'] = '_assets/images/is_page/'.$photo;
		                          $config['maintain_ratio'] = TRUE;
		                          $config['width']     = 900;
		                          $config['height']   = 600;
		                          $this->image_lib->initialize($config);
		                          $this->image_lib->resize();
		                          //-----------------------------
					}else{
						$photo = $photo_old;
					}
            	}
            }else{
						$photo = $photo_old;
					}
            
		$arr = array( 
					'head_title' =>$_POST['head_title'],
					'title' =>$_POST['title'],
					'content'=>$_POST['content'],
					'image'=>trim($photo)
					);
		$this->db->where('id',decode_id($_POST['ids']));
		$log=$this->db->update('is_page',$arr);
		// var_dump($this->db->last_query());
		// die();
		if($log){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil Rubah Data</div>');
			
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Edit Data Gagal !</div>');
		}
		redirect('admin/is_page');
	}

	public function delete($id){

		$this->db->where('id',decode_id($id));
		$pages=$this->db->get('is_page')->row(); 
		$filename = $pages->image
		;
                if (isset($filename)) {
                    $file = FCPATH.'assets/images/is_page/'.$filename;
                    if(file_exists($file))
                    unlink($file);
        }


		$this->db->where('id',decode_id($id));
		$log=$this->db->delete('is_page'); 
		if($log){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil Hapus Data</div>');
			
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Hapus Data Gagal !</div>');
		}
		redirect('admin/is_page');
	}


	public function ajax_upload_photo()
	  {
	     
	    // initialize FileUploader
	    $FileUploader = new FileUploader('file', array(
	        'limit' => 1,
	        'maxSize' => null,
	        'fileMaxSize' => 3000,
	        'extensions' => ['PNG','JPG','JPEG','jpg', 'jpeg', 'png', 'gif'],
	        'required' => false,
	        'uploadDir' => FCPATH.'assets/images/is_page/',
	        'title' => 'photo_{random}_{timestamp}',
	        'replace' => false,
	        'listInput' => false,
	        'files' => null
	    ));
	  
	    // call to upload the files
	    $data = $FileUploader->upload();
	    $arr = array('photo'=>$data['files'][0]['name']);
	    $this->db->set('created_at','NOW()',false);
	    $this->db->insert('image_temp',$arr);
	    echo json_encode($data);
	    exit;
	  }

	  public function ajax_remove_photo()
              {
                $filename = $_POST['file'];
                if (isset($filename)) {
                    $file = FCPATH.'assets/images/is_page/'.$filename;
                    if(file_exists($file))
                    unlink($file);
                }
              }


}
