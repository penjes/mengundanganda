<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galleri extends CI_Controller {
    function __construct()
    {
        parent::__construct();
           $this->load->library('image_lib');

        if ((empty($this->session->userdata('users')))&&(empty($this->session->userdata('users')))) {
            redirect('admin/login');
        }
         
    }
   
	 
	public function index()
	{
		$data['page']='galleri';
						 $this->db->order_by('id','desc');
		$data['photo'] = $this->db->get('galleri')->result();
		$this->load->view('dashboard/template',$data);
	}
	 

	public function add(){
		$acara=$this->db->get('galleri')->num_rows();
		
		$data = array(	'ids' =>'',
						'title' => '',
						'subtitle' => '',
						'head_title' => '',
						'page_name' => '' 
					);
		if(empty($acara)){
			$data['acara'] = 1;
		}else{
			$data['acara'] = $acara + 1;
		}
		$data['page']='galleri_add';
		$data['action'] = 'action_add';
		$this->load->view('dashboard/template',$data);
	}

public function ajax_upload_photo()
  {
     
    // initialize FileUploader
    $FileUploader = new FileUploader('file', array(
        'limit' => 8,
        'maxSize' => null,
        'fileMaxSize' => 8,
        'extensions' => ['PNG','JPG','JPEG','jpg', 'jpeg', 'png', 'gif'],
        'required' => false,
        'uploadDir' => FCPATH.'assets/images/galleri/',
        'title' => 'photo_{random}_{timestamp}',
        'replace' => false,
        'listInput' => false,
        'files' => null
    ));
  
    // call to upload the files
    $data = $FileUploader->upload();
    $arr = array('photo'=>$data['files'][0]['name']);
    $this->db->set('created_at','NOW()',false);
    $this->db->insert('image_temp',$arr);
    echo json_encode($data);
    exit;
  }


  function action_add(){
  	 if ((sizeof($_POST) > 0)) {
            $arr = NULL;
              foreach($_POST as $key => $val)
              {
                if(!is_array($val))
                { 
                  if ($key !='fileuploader-list-file') {
                if ($key !='id') {
                     $arr[$key] = trim($val); 
               }
                  }
                 
                }
              }
  	 
        
              for ($i=0; $i <  count($_POST['photo']) ; $i++) {
                      if (!empty($_POST['photo'][$i])) {
                      	//------ compress -------------

		                          $config['image_library'] = 'gd2';
		                          $config['source_image'] = '_assets/images/galleri/'.trim($_POST['photo'][$i]);
		                          $config['maintain_ratio'] = TRUE;
		                          $config['width']     = 900;
		                          $config['height']   = 600;
		                          $this->image_lib->initialize($config);
		                          $this->image_lib->resize();
		                          //-----------------------------
                      $this->db->where('photo',trim($_POST['photo'][$i]));
                      $this->db->delete('image_temp');
                      $arr =array('image'=>trim($_POST['photo'][$i]),'title'=>$_POST['title'][$i],'subtitle'=>$_POST['subtitle'][$i],'status'=>'1');

                      $this->db->set('upload_date','NOW()',false);
                      $log=$this->db->insert('galleri',$arr);
                      
                      }
                      
                    }

           if ($log) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success">Product berhasil di tambahkan</div>');
           }else{
            $this->session->set_flashdata('msg', '<div class="alert alert-danger">Produk gagal ditambahkan</div>');
           }
           redirect('admin/galleri');
           exit();
        }
         $this->session->set_flashdata('msg', '<div class="alert alert-danger">Foto gagal ditambahkan</div>');
         redirect('admin/galleri'); 
	}

	function delete($id){
		$this->db->where('id',decode_id($id));
		$key=$this->db->get('galleri')->row();

		$file = FCPATH.'assets/images/galleri/'.$key->image;
		if(file_exists($file)){
                            unlink($file);
        }
        $this->db->where('id',decode_id($id));
		$key=$this->db->delete('galleri');
		 $this->session->set_flashdata('msg', '<div class="alert alert-success">Foto Berhasil dihapus</div>');
         redirect('admin/galleri'); 

	}

	public function edit($id){
		$acara=$this->db->get('galleri')->num_rows();
		$this->db->where('id',decode_id($id));
		$key=$this->db->get('galleri')->row();
	
		$data = array(	'ids' =>encode_id($key->id),
						'title' => $key->title,
						'subtitle' => $key->subtitle,
						'photo_old' => $key->image 
					);
		if(empty($acara)){
			$data['acara'] = 1;
		}else{
			$data['acara'] = $acara + 1;
		}
		$data['page']='galleri_edit';
		$data['action'] = 'ubah_photo';
		$this->load->view('dashboard/template',$data);
	}


	function ubah_photo(){

  	 if ((sizeof($_POST) > 0)) {
  	 	$photo = NULL;
            $arr = NULL;
              foreach($_POST as $key => $val)
              {
                if(!is_array($val))
                { 
                  if ($key !='fileuploader-list-file') {
                if ($key !='id') {
                     $arr[$key] = trim($val); 
               }
                  }
                 
                }
              }
  	 
        	if(!empty($_POST['photo'])){
        		for ($i=0; $i <  count($_POST['photo']) ; $i++) {
                      if (!empty($_POST['photo'][$i])) {
	                    $this->db->where('photo',trim($_POST['photo'][$i]));
	                    $this->db->delete('image_temp');
	                    $photo = trim($_POST['photo'][$i]); 
                      }
                      
              }
        	}
              
            $photo_old = $_POST['photo_old'];
              if(!empty($photo)){     
                if(!empty($photo_old)){		
                		if($photo !== $photo_old){
                	  								$file = FCPATH.'assets/images/galleri/'.$photo_old;// hapus file di server             
          		                          if(file_exists($file)){
          		                            unlink($file);
          		                          }
          		                          //------ compress -------------

          		                          $config['image_library'] = 'gd2';
          		                          $config['source_image'] = '_assets/images/galleri/'.$photo;
          		                          $config['maintain_ratio'] = TRUE;
          		                          $config['width']     = 900;
          		                          $config['height']   = 600;
          		                          $this->image_lib->initialize($config);
          		                          $this->image_lib->resize();
          		                          //-----------------------------
          					}else{
          						$photo = $photo_old;
          					}
                }
              }else{
                    $photo = $photo_old;
              }
            $arr =array('image'=>trim($photo),'title'=>$_POST['title'],'subtitle'=>$_POST['subtitle']);
                      
            $this->db->where('id',decode_id($_POST['ids']));
            $this->db->set('upload_date','NOW()',false);
            $log=$this->db->update('galleri',$arr);
                      

           if ($log) {
            $this->session->set_flashdata('msg', '<div class="alert alert-success">Product berhasil di tambahkan</div>');
           }else{
            $this->session->set_flashdata('msg', '<div class="alert alert-danger">Produk gagal ditambahkan</div>');
           }
           redirect('admin/galleri');
           exit();
        }
         $this->session->set_flashdata('msg', '<div class="alert alert-danger">Foto gagal ditambahkan</div>');
         redirect('admin/galleri');


	}



public function ajax_remove_photo()
              {
                $filename = $_POST['file'];
                echo $filename;
                if (isset($filename)) {
                    $file = FCPATH.'assets/images/galleri/'.$filename;
                    if(file_exists($file))
                    unlink($file);
                }
              }

}
