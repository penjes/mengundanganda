<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reviews extends CI_Controller {
	function __construct()
    {
        parent::__construct();
           $this->load->library('image_lib');
           $this->load->library('pagination');
        if ((empty($this->session->userdata('users')))&&(empty($this->session->userdata('users')))) {
            redirect('admin/login');
        }
         
    }
	 
	function index(){
		$this->list(0);
	}	

	public function list($page=0)
	{
 	    // 
      //   $this->pagination->initialize($config);
		$limit = 10;
		$data['page']='reviews';
		 
		if((isset($_GET['search']))&&(empty($_GET['search']))){
			$this->db->like('nama',$_GET['search']);
			$this->db->or_like('reviews',$_GET['search']);
		}
		$this->db->limit($limit,$page);
		$data['reviews'] = $this->db->get("reviews")->result();
	    
	    $total_row = $this->db->get("reviews")->num_rows();
	    
	    $config["base_url"] = base_url()."admin/produk/listing";
	    $config["uri_segment"] = 3;
	    if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
	    $config['full_tag_open'] = '<ul class="pagination" id="pagenumber-list-ajax">';
	    $config['full_tag_close'] = '</ul>';
	    $config['first_link'] = '';
	    $config['first_tag_open'] = '';
	    $config['first_tag_close'] = '';
	    $config['last_link'] = '';
	    $config['last_tag_open'] = '';
	    $config['last_tag_close'] = '';
	    $config['next_link'] = 'Next &raquo;';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['prev_link'] = '&laquo; Prev';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    // $config['num_links'] = $total_row;
	    // $config['use_page_numbers'] = FALSE;
	    $config["total_rows"] = $total_row;
	    $data['no']=$page;
	    $this->pagination->initialize($config);
	    $data["pagination"] = $this->pagination->create_links();
	    

		$this->load->view('dashboard/template',$data);
	}

}