<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct()
    {
        parent::__construct();
           $this->load->library('image_lib');

        if ((empty($this->session->userdata('users')))&&(empty($this->session->userdata('users')))) {
            redirect('admin/login');
        }
         
    }
	 
	 
	public function index()
	{
		$data['page']='home';
		$this->load->view('dashboard/template',$data);
		
	}
}
