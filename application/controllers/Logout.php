<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logout extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');        

    }
    function index(){
    	$this->session->unset_userdata(array("users"=>"", "user_level"=>"","kode_internal"=>""));
        $this->session->sess_destroy();
    	redirect('login');
    }

}