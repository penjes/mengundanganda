<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class order extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');        
		$this->fpath  = "admin/";
		$this->load->helper('common_helper');
	
	}

	public function index()
	{	
		$paket = $this->input->get('paket');
		$data['menu'] = 'order';
		$this->load->view('front/partial/header',$data);
		$this->load->view('front/order');
		$this->load->view('front/partial/footer');
	}

}
