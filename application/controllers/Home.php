<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

	public function index()
	{
		$exp=$this->db->query('select "Y" from konfiguration where expired_date > now()')->row();

		// var_dump($exp);die();
		if(!empty($exp)){

			// echo 'wwoooi';
						  $this->db->select("`id`,
						  					 `bg`, 
						  					 `mempelai_pria`, 
						  					 `photo_pria`,
						  					  `mempelai_wanita`, 
						  					  `photo_wanita`, 
						  					  `ayah_pria`, 
						  					  `ibu_pria`, 
						  					  `ayah_wanita`, 
						  					  `ibu_wanita`,
						  					   `content_pria`,
						  					   `content_wanita`,
						  					   `acara_resepsi`,
						  					   date_format(`acara_resepsi`,'%Y-%m-%d')as acara,
						  					   date_format(`acara_resepsi`,'%d')as tgl,
						  					   date_format(`acara_resepsi`,'%M')as bln,
						  					   date_format(`acara_resepsi`,'%Y')as thn,
						  					   `start_at`,
						  					   `end_at`,
						  					   `email`,
						  					   `telp`,
						  					   `whatsapp`,
						  					    `alamat`,
						  					    `kecamatan`,
						  					    `kota`,
						  					    `latitude`,
						  					    `longtitude`,
						  					    `kode_internal`,
						  					    `status`,
						  					    `expired_date`,
						  					    `author_is`, `created_at`");
						  $this->db->where('status','1');	
			$data['biodata'] = $this->db->get('konfiguration')->row();

			$this->db->where('status','1');
			$data['slider']=$this->db->get('slider')->result();


			$notif=$this->db->query("select acara_resepsi from konfiguration where acara_resepsi > now()")->row();
			if(!empty($notif)){
				$data['notif']='y';
			}else{
				$data['notif']='n';
			}
					$this->db->order_by('id','ASC');
					$this->db->where('status','1');
			$data['galleri']=$this->db->get('galleri')->result();

					$this->db->order_by('id','ASC');
					$this->db->where('status','1');
			$data['is_page']=$this->db->get('is_page')->result_array();

							$this->db->order_by('urutan','ASC');
							$this->db->where('status','1');
			$data['our_story']=$this->db->get('our_story')->result();

									$this->db->select("`title`, `description`, `kode_internal`,`id`,
														DATE_FORMAT(event_date,'%W  %d') as isdate,
														DATE_FORMAT(event_date,'%M, %Y') as ismonth,
														DATE_FORMAT(start_at,'%I:%i  %p') as start_at,
														DATE_FORMAT(end_at,'%I:%i  %p') as end_at");
									$this->db->order_by('id','ASC');
			$wed=$this->db->get('wedding_event');
			$data['wedding_event']=$wed->result();
			$data['tot_wed'] = $wed->num_rows();

									$this->db->order_by('id','ASC');
			$rev=$this->db->get('reviews');
			$data['tot_rev']=$rev->num_rows();
			$data['reviews']=$rev->result();
		$this->load->view('front/template',$data);
		}
		else{
			$arr = array('status' => '0');
			$this->db->where('kode_internal !=','ISDEFAULT');
			$this->db->update('konfiguration',$arr);
			redirect('Not_found');
		}
		
	}

	function send(){
			  $this->db->where('expired_date >','now()',false);
			  $this->db->where('status','1');
			  $this->db->where('kode_internal !=','isdefault');
		$kode=$this->db->get('konfiguration')->row();
		if(!empty($kode)){
			$data = array('nama' =>$_POST['nama'],
							'reviews' => $_POST['kartu'],
							'kode_internal' =>$kode->kode_internal,
							'status'=>'1'
							);

				$this->db->set('created_at','NOW()',false);
			$log=$this->db->insert('reviews',$data);
			if($log){
				$this->session->set_flashdata('message', '<div class="alert alert-success">Kartu Ucapan berhasil dikirim</div>');
			}else{
				$this->session->set_flashdata('message', '<div class="alert alert-danger">Kartu Ucapan gagal terkirim</div>');
			}
		}else{
				$this->session->set_flashdata('message', '<div class="alert alert-danger">Kartu Ucapan gagal terkirim</div>');
		}
		redirect(site_url('home'));
	}

	function isdefault(){
		$this->load->view('default');		
	}





}
