<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class frontendHome extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');        
		$this->fpath  = "admin/";
		$this->load->helper('common_helper');
		// $data = 'configuration';

		

	}

	public function index()
	{	
		// $data = 'configuration';
		// $a = $this->contentful->get('entries','sys');
		// var_dump($a);
		// var_dump($a->$a['configuration']);
		// var_dump($a['fields']);
		$this->load->view('front/partial/header');
		$this->load->view('front/index');
		$this->load->view('front/partial/footer');
	}

}
